package com.interwebbingtechonolgy.nine_dragons_customer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.google.android.material.tabs.TabLayout;
import com.interwebbingtechonolgy.nine_dragons_customer.Activity.MainLandingActivity;
import com.interwebbingtechonolgy.nine_dragons_customer.Adapter.PagerAdapter;
import com.interwebbingtechonolgy.nine_dragons_customer.Fragments.EmailLoginFragment;
import com.interwebbingtechonolgy.nine_dragons_customer.Fragments.SignInFragment;
import com.interwebbingtechonolgy.nine_dragons_customer.app.AppConfig;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

public class MainActivity extends AppCompatActivity {
    public static boolean mIsUserLogIn = false;
    private Context mAppContext;


    ViewPager mPager;
    TabLayout tabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        FacebookSdk.sdkInitialize( getApplicationContext() );
        if (BuildConfig.DEBUG) {
            FacebookSdk.setIsDebugEnabled( true );
            FacebookSdk.addLoggingBehavior( LoggingBehavior.INCLUDE_ACCESS_TOKENS );
        }
        setContentView( R.layout.activity_main );

        SharedPreferences sharePrefs = getSharedPreferences( "MYPREFERENCES", MODE_PRIVATE );
        mIsUserLogIn = sharePrefs.getBoolean( "islogin", false );
        SignInFragment.getUserName = sharePrefs.getString( "name", null );
        SignInFragment.getPassword = sharePrefs.getString( "password", null );
        SignInFragment.companyId = sharePrefs.getString( "companyid", null );

        EmailLoginFragment.getUserName = sharePrefs.getString( "ename", null );
        EmailLoginFragment.getPassword = sharePrefs.getString( "epassword", null );
        EmailLoginFragment.memberId = sharePrefs.getString( "ememberid", null );
        EmailLoginFragment.companyId = sharePrefs.getString( "ecompanyid", null );
        EmailLoginFragment.nrc = sharePrefs.getString( "enrc", null );


        if (mIsUserLogIn == false) {
            initOutOfSessionActivity( savedInstanceState );
        } else {
            initInSessionActivity( savedInstanceState );
        }
        mAppContext = getApplication();


        PDAppSession.getInstance( mAppContext, new AppConfig( mAppContext ) ).initialize();
        mPager = (ViewPager) findViewById( R.id.vpagercomfirm );
        setUpWithViewPager( mPager );
        tabLayout = (TabLayout) findViewById( R.id.tabLayoutComfirm );
        tabLayout.setupWithViewPager( mPager );


    }

    private void setUpWithViewPager(ViewPager mPager) {

        PagerAdapter pagerAdaper = new PagerAdapter( getSupportFragmentManager() );

        pagerAdaper.addFragment( new SignInFragment(), getString( R.string.nrc ) );
        pagerAdaper.addFragment( new EmailLoginFragment(), getString( R.string.email ) );


        mPager.setAdapter( pagerAdaper );

    }


    private void initInSessionActivity(Bundle savedInstanceState) {
        Intent intent = new Intent( MainActivity.this, MainLandingActivity.class );
        startActivity( intent );
        finish();
    }

    private void initOutOfSessionActivity(Bundle savedInstanceState) {
        setContentView( R.layout.activity_main );
        if (isNetworkAvailable() == false) {
            Toast.makeText( mAppContext, R.string.network_error, Toast.LENGTH_SHORT ).show();
        }
        if (savedInstanceState == null) {
            loadSignInFragment();
        }
    }

    private void loadSignInFragment() {
//    FragmentManager fragmentManager = getSupportFragmentManager();
//       fragmentManager.beginTransaction().replace(R.id.fragment_container, new SignInFragment()).commit();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplication().getSystemService( Context.CONNECTIVITY_SERVICE );
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void switchToSignInMode(String memberId) {
        initInSessionActivity( null );
        mIsUserLogIn = true;
    }
}
