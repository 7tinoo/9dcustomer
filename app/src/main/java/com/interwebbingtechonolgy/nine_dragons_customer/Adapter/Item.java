package com.interwebbingtechonolgy.nine_dragons_customer.Adapter;

/**
 * Created by smart05 on 6/22/17.
 */


import com.interwebbingtechonolgy.android_commons_commerce.model.AllItemResponse;
import com.interwebbingtechonolgy.android_commons_commerce.model.ShowMemberResponse;
import com.interwebbingtechonolgy.android_commons_commerce.model.ShowVouncherResponse;

/**
 * Created by smart05 on 6/22/17.
 */
public class Item {

    int Image;
    AllItemResponse deliverData;
    String noData;

    ShowMemberResponse showMemberResponse;


    ShowVouncherResponse showVouncherResponse;

    public ShowVouncherResponse getShowVouncherResponse() {
        return showVouncherResponse;
    }

    public void setShowVouncherResponse(ShowVouncherResponse showVouncherResponse) {
        this.showVouncherResponse = showVouncherResponse;
    }


    public ShowMemberResponse getShowMemberResponse() {
        return showMemberResponse;
    }

    public void setShowMemberResponse(ShowMemberResponse showMemberResponse) {
        this.showMemberResponse = showMemberResponse;
    }


    public Item(ShowMemberResponse showMemberResponse) {

        this.showMemberResponse=showMemberResponse;

    }

    public Item(ShowVouncherResponse showVouncherResponse) {

        this.showVouncherResponse=showVouncherResponse;

    }

    public Item(int Image, String noData) {
        this.Image=Image;
        this.noData=noData;

    }

    public int getImage()
    {
        return Image;
    }
    public AllItemResponse getDeliverData() {
        return deliverData;
    }
    public void setImage(int image) {
        this.Image = image;
    }
    public void setDeliverData(AllItemResponse deliverData) {
        this.deliverData = deliverData;
    }



}









