package com.interwebbingtechonolgy.nine_dragons_customer.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.interwebbingtechonolgy.android_commons_commerce.model.VouncherList;
import com.interwebbingtechonolgy.nine_dragons_customer.Activity.VDetailActivity;
import com.interwebbingtechonolgy.nine_dragons_customer.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class VouncherListAdapter extends RecyclerView.Adapter<VouncherListAdapter.MyViewHolder> {

    List<Item> itemList;
    ArrayList<Item> memberDataArrayList;
    Context context;
    VouncherList[] vouncherLists;

    public VouncherListAdapter(List<Item> memberData, Context context) {
        this.memberDataArrayList = new ArrayList<>();
        itemList = memberData;
        this.context = context;
        this.memberDataArrayList.addAll( itemList );
        // EventBus.getDefault().register( this );
    }

    @NonNull

    @Override
    public VouncherListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from( context ).inflate( R.layout.vouncher_list, viewGroup, false );


        return new VouncherListAdapter.MyViewHolder( v );
    }

    @Override
    public void onBindViewHolder(@NonNull VouncherListAdapter.MyViewHolder myViewHolder, final int i) {

        // EventBus.getDefault().register( this );
        //  myViewHolder.vouncherId.setText( data.get( i ).getVouncherId() );

//
//        for (int in = 0; in < itemList.get( i ).getDeliverData().getLength().length(); in++) {
//            myViewHolder.tvName.setText( itemList.get( i ).getDeliverData().getItemName()  );
//
//
//
//
//        }

        //Log.d( "iiiiiiiii", itemList.get( i ).showVouncherResponse.get );
        myViewHolder.totalPrice.setText( itemList.get( i ).showVouncherResponse.getTotalPrice() + " ကျပ်" );

        myViewHolder.vouncherId.setText( itemList.get( i ).showVouncherResponse.getVouncherId() );

        myViewHolder.cardView.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent( context, VDetailActivity.class );
                intent.putExtra( "id", itemList.get( i ).getShowVouncherResponse() );
                intent.putExtra( "date", itemList.get( i ).getShowVouncherResponse().getCreatedAt() );
                intent.putExtra( "helloid", itemList.get( i ).getShowVouncherResponse().getVouncherId() );
                //  intent.putExtra( "name",vouncherLists);
                context.startActivity( intent );


//                Toast.makeText( context, "Touch", Toast.LENGTH_SHORT ).show();
//                Intent intent = new Intent(context, VouncherDetailActivity.class);
//
//
//                intent.putExtra("name",data.get(i).getItemName());
//                intent.putExtra("image",data.get(i).getItemImage());
//
//
//
//                if(data.get(i).getPromotion()!=null){
//                    intent.putExtra("prom",data.get(i).getPromotion());
//
//                }
//                else intent.putExtra( "prom","ပရိုမိုးရှင်း မရှိပါ" );
//
//                intent.putExtra("price",data.get(i).getPrice());
//                intent.putExtra("weight",data.get(i).getFinalGram());
//                intent.putExtra("qty",data.get(i).getQuality());
//                intent.putExtra("code",data.get(i).getItemCode());
//
//
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
//                context.startActivity(intent);


            }
        } );
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView vouncherId, totalPrice, tvName;

        CardView cardView;

        public MyViewHolder(@NonNull View itemView) {
            super( itemView );

            vouncherId = itemView.findViewById( R.id.tv_vcId );
            totalPrice = itemView.findViewById( R.id.tv_tprice );
            // tvName = itemView.findViewById( R.id.tv_name );
            cardView = itemView.findViewById( R.id.cv_member );
        }
    }


    public void filter(String charText) {
        charText = charText.toLowerCase( Locale.getDefault() );
        itemList.clear();
        if (charText.length() == 0) {
            itemList.addAll( memberDataArrayList );
        } else {
            for (Item filter : memberDataArrayList) {
                if (filter.getShowVouncherResponse().getVouncherId().toLowerCase( Locale.getDefault() ).contains( charText )) {
                    itemList.add( filter );
                }
            }
        }
        notifyDataSetChanged();
    }


}
