package com.interwebbingtechonolgy.nine_dragons_customer.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.interwebbingtechonolgy.android_commons_commerce.model.VouncherData;
import com.interwebbingtechonolgy.nine_dragons_customer.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by smart01 on 2/27/18.
 */

public class VouncherAdapter extends ArrayAdapter<VouncherData> {

    Context mContext;
    public ImageView imageView;
    private static ArrayList<VouncherData> cartDataArrayList;

    public VouncherAdapter(Context context, int list_view_item, ArrayList<VouncherData> cartDatas) {

        super (context, list_view_item, cartDatas);
        mContext=context;
        cartDataArrayList =cartDatas;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)  {
        final CartListItemViewHolder viewHolder;
        View v = convertView;
        if (v == null) {
            v = LayoutInflater.from(getContext()).inflate(R.layout.cart_list_vouncher, null);
            viewHolder = new CartListItemViewHolder(v);
        } else {
            viewHolder = (CartListItemViewHolder) v.getTag();
        }
        v.setTag(viewHolder);
     //   v.setTag( R.id.itemName, viewHolder.cartItemId);
        viewHolder.itemName.setText(cartDataArrayList.get(position).getAllItemData().get(position).getItemName());
        viewHolder.cartItemId.setText(cartDataArrayList.get(position).getAllItemData().get(position).getItemCode());
        viewHolder.price.setText(cartDataArrayList.get(position).getAllItemData().get(position).getPrice()+ "ကျပ်");
        Picasso.with(getContext())
                .load(cartDataArrayList.get(position).getAllItemData().get(position).getItemImage())
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher)
                .into(viewHolder.cartItemImage);

        viewHolder.original.setText(cartDataArrayList.get(position).getAllItemData().get(position).getFinalGram());
        viewHolder.reduce.setText(cartDataArrayList.get(position).getAllItemData().get(position).getOriginalReduceIndividual());
        viewHolder.externalFee.setText(cartDataArrayList.get(position).getAllItemData().get(position).getExternalFee() + "ကျပ်" );
        viewHolder.showLength.setText(cartDataArrayList.get(position).getAllItemData().get(position).getLength() + "\t"+mContext.getString(R.string.unit_length));
      //Log.d("reduceGold", cartDataArrayList.get(position).getAllItemData().get(position).getOriginalReduceIndividual());
        viewHolder.reduceGold.setText(cartDataArrayList.get(position).getAllItemData().get(position).getReduce());
//        viewHolder.reduceGold.setText(CartListAdapter.ArrayList2.get(position));
        return v;

    }

    public class CartListItemViewHolder {
        public TextView cartItemId, itemName, priceText2,reduce, price,original,externalFee,showLength,reduceGold;
        public ImageView cartItemImage;
        public Button btn;
        public EditText priceEditText;

        public CartListItemViewHolder(View view) {
            cartItemId = (TextView) view.findViewById(R.id.cart_item_id);
            cartItemImage = (ImageView) view.findViewById(R.id.cart_item_image);
            itemName = (TextView) view.findViewById(R.id.item_name);
            priceText2 = (TextView) view.findViewById(R.id.price);
            original=(TextView)view.findViewById(R.id.gold_value);
            price= (TextView) view.findViewById(R.id.price);
            reduce= (TextView) view.findViewById(R.id.reduce_we);
            externalFee= (TextView) view.findViewById(R.id.fee);
            reduceGold=(TextView)view.findViewById(R.id.reduce_we);
            showLength = (TextView) view.findViewById(R.id.length_value);

        }
    }



}
