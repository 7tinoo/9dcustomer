package com.interwebbingtechonolgy.nine_dragons_customer.app;

import android.content.Context;

import com.interwebbingtechonolgy.nine_dragons_customer.Common.AppConfigBase;


/**
 * Created by smart04 on 6/12/17.
 */

public class AppConfig extends AppConfigBase {
    public AppConfig(Context appContext)
    {

    }

    public AppConfig()
    {

    }

    public String getConnectionEncryptionPassword() {
        return "replace_me from config later";
    }

    public String getConnectionEncryptionSalt() {
        return "salt, replace me later";
    }
}
