package com.interwebbingtechonolgy.nine_dragons_customer.app;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.interwebbingtechonolgy.android_commons_commerce.preferences.SharedPreferencesManager;
import com.interwebbingtechonolgy.android_commons_commerce.preferences.SharedPreferencesModule;
import com.interwebbingtechonolgy.nine_dragons_customer.utils.encryption.StringEncryptor;


/**
 * Created by smart04 on 6/12/17.
 */

public class Preferences {
    private static final String SHARED_PREFERENCES_FILE_NAME = "com.ewf.preferences";
    private static final String PREF_EWF_API_AUTH_TOKEN = "com.ewf.preferences.user.authtoken";
    private static final String PREF_EWF_API_AUTH_TOKEN_EXPIRATION = "com.ewf.preferences.user.authtoken.expiration";
    private static final String PREF_EWF_API_EMAIL = "com.ewf.preferences.user.email";
    private static final String PREF_EWF_API_USERID = "com.ewf.preferences.user.userid";
    private static final String PREF_EWF_API_USER_PWD = "com.ewf.preferences.user.password";
    private static final String PREF_EWF_API_FIRST_NAME = "com.ewf.preferences.user.firstname";
    private static final String PREF_EWF_API_LAST_NAME = "com.ewf.preferences.user.lastname";
    private static final String PREF_EWF_ENCRYPTION_STATE= "com.ewf.preferences.encryption.state";

    private static SharedPreferencesModule sharedPreferences = null;
    private static SharedPreferencesModule appPreferences = null;
    private static PackageInfo sPackageInfo;

    private static StringEncryptor sLocalTextEncryptor;

    private enum EncryptionState
    {
        NO_ENCRYPTION(0x00000000),
        EWF_ENCRYPTION(0x00000001);
        private final int state;
        EncryptionState(int state) {
            this.state = state;
        }
    }

    public static void initPreferences(Context appContext, AppConfig appConfig)
    {
        sharedPreferences = SharedPreferencesManager.getInstance().createSharedPreferenceModule(appContext, SHARED_PREFERENCES_FILE_NAME);
        appPreferences = SharedPreferencesManager.getInstance().createSharedPreferenceModule(appContext);
        try {
            sPackageInfo = appContext.getPackageManager().getPackageInfo(appContext.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    public static String getEwfAuthToken()
    {
        return sharedPreferences.get(PREF_EWF_API_AUTH_TOKEN, "");
    }

    public static String getEwfAuthTokenExpiration()
    {
        return sharedPreferences.get(PREF_EWF_API_AUTH_TOKEN_EXPIRATION, "");
    }

    public static String getEwfAuthAPIEmail()
    {
        return sharedPreferences.get(PREF_EWF_API_EMAIL, "");
    }

    public static String getEwfAuthUserId()
    {
        return sharedPreferences.get(PREF_EWF_API_USERID, "");
    }

    public static void setupEncryptorObjectIfNeeded(Context appContext, AppConfig appConfig)
    {
        try{
            if(sLocalTextEncryptor == null) {
                //sLocalTextEncryptor = new StringEncryptor(appConfig.)
            }
        } catch(Exception e) {

        }
    }
    public static String getEwfAuthApiPassword()
    {
        String encrypedString = sharedPreferences.get(PREF_EWF_API_USER_PWD, "");
        int encryptionState = getEncryptionState();
        try {
            if(sLocalTextEncryptor != null )
            {
                //PreferenceManager.setDefaultValues(appContext, R.xml.);

                return sLocalTextEncryptor.decrypt(encrypedString);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "DLKJFSO";
    }

    private static void setEncryptionState(int state) {
        sharedPreferences.put(PREF_EWF_ENCRYPTION_STATE, state);
    }
    private static int getEncryptionState() {
        return sharedPreferences.get(PREF_EWF_ENCRYPTION_STATE, EncryptionState.NO_ENCRYPTION.state);
    }
}
