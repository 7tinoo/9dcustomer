package com.interwebbingtechonolgy.nine_dragons_customer.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.interwebbingtechonolgy.android_commons_commerce.event.ShowVouncherReceivedDetailEvent;
import com.interwebbingtechonolgy.android_commons_commerce.manager.ShowVouncherDetailManager;
import com.interwebbingtechonolgy.android_commons_commerce.model.ShowVouncherDetailResponse;
import com.interwebbingtechonolgy.nine_dragons_customer.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.facebook.FacebookSdk.getApplicationContext;


public class BuyFragment extends Fragment {

    TextView name, nrc, phone, address, level, errMsg, totalPrice;
    ImageView imageView;
    TableLayout itemReportTable;
    View view;
    ArrayList<ShowVouncherDetailResponse> transactionResponseArrayList = new ArrayList<>();
    double total_price = 0.0;
    private String TAG = "BuyFragment";
    private ProgressDialog progressDialog;
    private RelativeLayout relativeLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate( R.layout.fragment_item_list, container, false );
        if (!EventBus.getDefault().isRegistered( this )) {
            EventBus.getDefault().register( this );
        }

        itemReportTable = (TableLayout) view.findViewById( R.id.itemReportTable );
        totalPrice = view.findViewById( R.id.tv_total );
        relativeLayout = view.findViewById( R.id.relativeLayout );
        errMsg = view.findViewById( R.id.tv_errormsg );

        SignInFragment.companyId = EmailLoginFragment.companyId;
        SignInFragment.getUserName = EmailLoginFragment.nrc;

//        ShowVouncherDetailManager.getInstance().fetchAllVouncherList("dcd1f215-bf29-49cf-b000-4f089fb1dfb9",SignInFragment.getUserName);
        // ShowVouncherDetailManager.getInstance().fetchAllVouncherList( EmailLoginFragment.companyId, EmailLoginFragment.nrc );
        ShowVouncherDetailManager.getInstance().fetchAllVouncherList( SignInFragment.companyId, SignInFragment.getUserName );


        Log.d( "eeeeeeeee", String.valueOf( EmailLoginFragment.companyId ) );
        Log.d( "eeeeeeeeennn", String.valueOf( EmailLoginFragment.nrc ) );
        Log.d( "eeeeeeeennnnnnnn", String.valueOf( SignInFragment.getUserName ) );


        if (progressDialog == null) {
            progressDialog = new ProgressDialog( getContext() );
            progressDialog.setTitle( R.string.login_body );
            progressDialog.setMessage( getResources().getString( R.string.login_body ) );
            progressDialog.show();
        }

        return view;
    }

    public void onEvent(ShowVouncherReceivedDetailEvent showVouncherReceivedEvent) {
        Log.d( "hklymh6yhyh", SignInFragment.getUserName );
        final List<ShowVouncherDetailResponse> showVouncherDetailResponseList = showVouncherReceivedEvent.getShowVouncherResponse();
        transactionResponseArrayList.clear();

        if (showVouncherDetailResponseList.size() != 0) {
            progressDialog.dismiss();
            relativeLayout.setVisibility( View.VISIBLE );
            transactionResponseArrayList.addAll( showVouncherDetailResponseList );

            for (int i = 0; i < transactionResponseArrayList.size(); i++) {
                TableRow tableRow = new TableRow( getContext() );

                if (i % 2 == 0) {
                    tableRow.setBackgroundColor( getResources().getColor( R.color.colorSecondary ) );
                } else {
                    tableRow.setBackgroundColor( getResources().getColor( R.color.colorSecondaryLight ) );
                }

                itemReportTable.addView( tableRow );
                for (int j = 0; j < 1; j++) {
                    String image = transactionResponseArrayList.get( i ).getItemImage();
                    CircleImageView imageView = new CircleImageView( getActivity() );
                    Picasso.with( getContext() ).load( image ).resize( 150, 150 ).centerCrop().into( imageView );
                    tableRow.setPadding( 8, 8, 8, 8 );
                    tableRow.addView( imageView );
                }

                // tableRow.addView(createTextView(transactionResponseArrayList.get(i).getItemImage()+"\n"+transactionResponseArrayList.get(i).getItemCode()));
                tableRow.addView( createTextView( transactionResponseArrayList.get( i ).getItemCode() + "\n" + transactionResponseArrayList.get( i ).getItemName() ) );
                tableRow.addView( createTextView( transactionResponseArrayList.get( i ).getQuality() ) );
                tableRow.addView( createTextView( transactionResponseArrayList.get( i ).getFinalGram() ) );
                tableRow.addView( createTextView( transactionResponseArrayList.get( i ).getPrice() ) );

                total_price += Double.parseDouble( transactionResponseArrayList.get( i ).getPrice() );
            }
            totalPrice.setText( "Ks " + total_price );
            Log.e( TAG, String.valueOf( total_price ) );
        } else {
            progressDialog.dismiss();
            System.out.println( "ERROR1" );
            errMsg.setText( getString( R.string.noData ) );
            errMsg.setVisibility( View.VISIBLE );
        }

    }


    public View createTextView(String text) {
        TextView tableEntry = new TextView( getApplicationContext() );
        tableEntry.setText( text );
        tableEntry.setPadding( 35, 8, 1, 8 );

        tableEntry.setTextSize( 14 );
        tableEntry.setTextColor( getResources().getColor( R.color.com_facebook_blue ) );

        return tableEntry;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (EventBus.getDefault().isRegistered( this )) {
            EventBus.getDefault().unregister( this );
        }
    }

    public void loadImageFromoUrl(String src) {

        Picasso.with( getActivity() ).load( src ).placeholder( R.mipmap.ic_launcher )
                .error( R.mipmap.ic_launcher )
                .into( imageView, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                    }
                } );
    }
}
