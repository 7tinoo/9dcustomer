package com.interwebbingtechonolgy.nine_dragons_customer.Fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.interwebbingtechonolgy.android_commons_commerce.event.SignInReceivedEvent;
import com.interwebbingtechonolgy.android_commons_commerce.manager.SignInManager;
import com.interwebbingtechonolgy.android_commons_commerce.model.MemberData;
import com.interwebbingtechonolgy.nine_dragons_customer.Activity.CreatePasswordActivity;
import com.interwebbingtechonolgy.nine_dragons_customer.MainActivity;
import com.interwebbingtechonolgy.nine_dragons_customer.R;
import com.jaredrummler.materialspinner.MaterialSpinner;

import androidx.fragment.app.Fragment;
import de.greenrobot.event.EventBus;

public class SignInFragment extends Fragment {

    MaterialSpinner spinnerNo, spinnerName, spinnerCode;

    private EditText usernameEditText, passwordEditText;
    private Button loginButton;
    private TextView forgetPwdTextView, showPwdTextView;
    public static String getUserName, getPassword;
    private MemberData memberData;
    private ImageView imageView;
    public static String memberId, name, selfiephoto;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    ProgressDialog progressDialog;
    public static String companyId;
    String usertext;
    String userspinnerNo;
    String userspinnerName;
    String userspinnerCode;
    String usernameAll;
    private boolean passwordVisible = true;
    private static java.util.regex.Pattern NRC_PATTERN = java.util.regex.Pattern.compile( "([0-9]{6})" );
    private String TAG = "SignInFragment";

    public SignInFragment() {
        if (!EventBus.getDefault().isRegistered( this )) {
            EventBus.getDefault().register( this );
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate( R.layout.fragment_sign_in, container, false );

        return view;
    }


    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated( savedInstanceState );
        View view = getView();

        usernameEditText = view.findViewById( R.id.edit_username );
        passwordEditText = view.findViewById( R.id.edit_password );
        loginButton = view.findViewById( R.id.btnsignin );
        forgetPwdTextView = view.findViewById( R.id.tvforget_pwd );
        imageView = view.findViewById( R.id.imv_logo );
        showPwdTextView = view.findViewById( R.id.tv_showPwd );
        spinnerNo = view.findViewById( R.id.spinner_nrc_no );
        spinnerName = view.findViewById( R.id.spinner_nrc_name );
        spinnerCode = view.findViewById( R.id.spinner_nrc_code );

        String[] nrcNo = getResources().getStringArray( R.array.array_nrc_no );

        ArrayAdapter<String> adapter = new ArrayAdapter<String>( getContext(), R.layout.spinner_item, R.id.text, nrcNo );
        adapter.setDropDownViewResource( R.layout.spinner_dropdown_item );
        spinnerNo.setAdapter( adapter );

//        spinnerNo.setOnItemSelectedListener( new MaterialSpinner.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                Toast.makeText( getContext(), String.valueOf( item ), Toast.LENGTH_LONG).show();
//            }
//        } );


        String[] nrc_code = getResources().getStringArray( R.array.nrc_code );
        final ArrayAdapter<String> adapter_code = new ArrayAdapter<String>( getContext(), R.layout.spinner_item, R.id.text, nrc_code );
        adapter_code.setDropDownViewResource( R.layout.spinner_dropdown_item );
        spinnerCode.setAdapter( adapter_code );

        String[] nrc1 = getResources().getStringArray( R.array.array_nrc_name_1 );
        final ArrayAdapter<String> adapter1 = new ArrayAdapter<String>( getContext(), R.layout.spinner_item, R.id.text, nrc1 );
        adapter.setDropDownViewResource( R.layout.spinner_dropdown_item );
        spinnerName.setAdapter( adapter1 );


        String[] nrc2 = getResources().getStringArray( R.array.array_nrc_name_2 );
        final ArrayAdapter<String> adapter2 = new ArrayAdapter<String>( getContext(), R.layout.spinner_item, R.id.text, nrc2 );
        adapter2.setDropDownViewResource( R.layout.spinner_dropdown_item );

        String[] nrc3 = getResources().getStringArray( R.array.array_nrc_name_3 );
        final ArrayAdapter<String> adapter3 = new ArrayAdapter<String>( getContext(), R.layout.spinner_item, R.id.text, nrc3 );
        adapter3.setDropDownViewResource( R.layout.spinner_dropdown_item );

        String[] nrc4 = getResources().getStringArray( R.array.array_nrc_name_4 );
        final ArrayAdapter<String> adapter4 = new ArrayAdapter<String>( getContext(), R.layout.spinner_item, R.id.text, nrc4 );
        adapter4.setDropDownViewResource( R.layout.spinner_dropdown_item );

        String[] nrc5 = getResources().getStringArray( R.array.array_nrc_name_5 );
        final ArrayAdapter<String> adapter5 = new ArrayAdapter<String>( getContext(), R.layout.spinner_item, R.id.text, nrc5 );
        adapter5.setDropDownViewResource( R.layout.spinner_dropdown_item );

        String[] nrc6 = getResources().getStringArray( R.array.array_nrc_name_6 );
        final ArrayAdapter<String> adapter6 = new ArrayAdapter<String>( getContext(), R.layout.spinner_item, R.id.text, nrc6 );
        adapter6.setDropDownViewResource( R.layout.spinner_dropdown_item );

        String[] nrc7 = getResources().getStringArray( R.array.array_nrc_name_7 );
        final ArrayAdapter<String> adapter7 = new ArrayAdapter<String>( getContext(), R.layout.spinner_item, R.id.text, nrc7 );
        adapter7.setDropDownViewResource( R.layout.spinner_dropdown_item );

        String[] nrc8 = getResources().getStringArray( R.array.array_nrc_name_8 );
        final ArrayAdapter<String> adapter8 = new ArrayAdapter<String>( getContext(), R.layout.spinner_item, R.id.text, nrc8 );
        adapter8.setDropDownViewResource( R.layout.spinner_dropdown_item );

        String[] nrc9 = getResources().getStringArray( R.array.array_nrc_name_9 );
        final ArrayAdapter<String> adapter9 = new ArrayAdapter<String>( getContext(), R.layout.spinner_item, R.id.text, nrc9 );
        adapter9.setDropDownViewResource( R.layout.spinner_dropdown_item );

        String[] nrc10 = getResources().getStringArray( R.array.array_nrc_name_10 );
        final ArrayAdapter<String> adapter10 = new ArrayAdapter<String>( getContext(), R.layout.spinner_item, R.id.text, nrc10 );
        adapter10.setDropDownViewResource( R.layout.spinner_dropdown_item );

        String[] nrc11 = getResources().getStringArray( R.array.nrc_11 );
        final ArrayAdapter<String> adapter11 = new ArrayAdapter<String>( getContext(), R.layout.spinner_item, R.id.text, nrc11 );
        adapter11.setDropDownViewResource( R.layout.spinner_dropdown_item );

        String[] nrc12 = getResources().getStringArray( R.array.nrc_12 );
        final ArrayAdapter<String> adapter12 = new ArrayAdapter<String>( getContext(), R.layout.spinner_item, R.id.text, nrc12 );
        adapter12.setDropDownViewResource( R.layout.spinner_dropdown_item );

        String[] nrc13 = getResources().getStringArray( R.array.nrc_13 );
        final ArrayAdapter<String> adapter13 = new ArrayAdapter<String>( getContext(), R.layout.spinner_item, R.id.text, nrc13 );
        adapter13.setDropDownViewResource( R.layout.spinner_dropdown_item );

        String[] nrc14 = getResources().getStringArray( R.array.nrc_14 );
        final ArrayAdapter<String> adapter14 = new ArrayAdapter<String>( getContext(), R.layout.spinner_item, R.id.text, nrc14 );
        adapter14.setDropDownViewResource( R.layout.spinner_dropdown_item );


        spinnerNo.setOnItemSelectedListener( new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                if (String.valueOf( position ).equals( "0" )) {
                    spinnerName.setAdapter( adapter1 );
                } else if (String.valueOf( position ).equals( "1" )) {
                    spinnerName.setAdapter( adapter2 );
                } else if (String.valueOf( position ).equals( "2" )) {
                    spinnerName.setAdapter( adapter3 );
                } else if (String.valueOf( position ).equals( "3" )) {
                    spinnerName.setAdapter( adapter4 );
                } else if (String.valueOf( position ).equals( "4" )) {
                    spinnerName.setAdapter( adapter5 );
                } else if (String.valueOf( position ).equals( "5" )) {
                    spinnerName.setAdapter( adapter6 );
                } else if (String.valueOf( position ).equals( "6" )) {
                    spinnerName.setAdapter( adapter7 );
                } else if (String.valueOf( position ).equals( "7" )) {
                    spinnerName.setAdapter( adapter8 );
                } else if (String.valueOf( position ).equals( "8" )) {
                    spinnerName.setAdapter( adapter9 );
                } else if (String.valueOf( position ).equals( "9" )) {
                    spinnerName.setAdapter( adapter10 );
                } else if (String.valueOf( position ).equals( "10" )) {
                    spinnerName.setAdapter( adapter11 );
                } else if (String.valueOf( position ).equals( "11" )) {
                    spinnerName.setAdapter( adapter12 );
                } else if (String.valueOf( position ).equals( "12" )) {
                    spinnerName.setAdapter( adapter13 );
                } else if (String.valueOf( position ).equals( "13" )) {
                    spinnerName.setAdapter( adapter14 );
                }
                userspinnerNo = String.valueOf( spinnerNo.getItems().get( position ) );
            }
        } );

        spinnerName.setOnItemSelectedListener( new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                userspinnerName = String.valueOf( spinnerName.getItems().get( position ) );

            }
        } );

        spinnerCode.setOnItemSelectedListener( new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                userspinnerCode = String.valueOf( spinnerCode.getItems().get( position ) );

            }
        } );

        usernameEditText.addTextChangedListener( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                usertext = userspinnerNo + "/" + userspinnerName + userspinnerCode + usernameEditText.getText().toString();
                getUserName = usertext;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        } );


        sharedPreferences = getActivity().getSharedPreferences( "MYPREFERENCES", Context.MODE_PRIVATE );

        loginButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable() == true) {
                    signIn();
                } else {
                    Toast.makeText( getActivity(), R.string.network_error, Toast.LENGTH_SHORT ).show();
                }
            }
        } );

        passwordEditText.setOnFocusChangeListener( new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                String getNrc = usernameEditText.getText().toString();
                if (!isNrcValid( getNrc )) {
                    usernameEditText.setError( getString( R.string.validation_nrc_format ) );
                    usernameEditText.requestFocus();
                }
            }
        } );


        passwordEditText.setOnKeyListener( new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN &&
                        keyCode == KeyEvent.KEYCODE_ENTER) {
                    SignInManager.getInstance().isLogin( usertext, getPassword );
                    return true;
                }
                return false;
            }
        } );

        //passwordToggleVisible
        passwordEditText.setInputType( InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD );
        passwordEditText.addTextChangedListener( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (passwordEditText.getText().length() > 0) {
                    showPwdTextView.setVisibility( View.VISIBLE );
                } else if (passwordEditText.getText().length() == 0) {
                    showPwdTextView.setVisibility( View.GONE );
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (passwordEditText.getText().length() > 0) {
                    showPwdTextView.setVisibility( View.VISIBLE );
                } else if (passwordEditText.getText().length() == 0) {
                    showPwdTextView.setVisibility( View.GONE );
                }
            }
        } );

        showPwdTextView.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showPwdTextView.getText().equals( "SHOW" )) {
                    showPwdTextView.setText( "HIDE" );
                    passwordEditText.setInputType( InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD );
                    passwordEditText.setSelection( passwordEditText.length() );
                } else {
                    showPwdTextView.setText( "SHOW" );
                    passwordEditText.setInputType( InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD );
                    passwordEditText.setSelection( passwordEditText.length() );
                }
            }
        } );


    }

    private void signIn() {
//        Log.d( "uuuuuuuuuuu", (usertext) );
        getPassword = passwordEditText.getText().toString();

        if (progressDialog == null) {
            progressDialog = new ProgressDialog( getContext() );
            progressDialog.setTitle( R.string.login_title );
            progressDialog.setMessage( getResources().getString( R.string.login_body ) );
            progressDialog.show();
        }

        if (getUserName.equals( "" ) || getPassword.equals( "" )) {
            Toast.makeText( getActivity(), R.string.fill_field, Toast.LENGTH_SHORT ).show();
            progressDialog.dismiss();
        } else {
            Log.d( "kkkkkkkkkk", (getUserName) );
            SignInManager.getInstance().isLogin( getUserName, getPassword );
        }

    }

    boolean isNrcValid(CharSequence nrc) {
        if (!TextUtils.isEmpty( nrc )) {
            return NRC_PATTERN.matcher( nrc ).matches();
        }
        return false;
    }


    public void onEvent(SignInReceivedEvent signInReceivedEvent) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        memberData = signInReceivedEvent.getMemberData();
        if (signInReceivedEvent.getMemberData() != null && signInReceivedEvent.isSuccess()) {
            String user_name = memberData.getMemberNrc();
            String passworD = memberData.getMemberPassword();
            memberId = memberData.getMemberId();
            name = memberData.getMemberName();
            selfiephoto = memberData.getMemberImage();
            companyId = memberData.getCompanyId();

            if (getUserName.equals( user_name ) && passworD.equals( "9dpass" )) {
                Intent intent = new Intent( getActivity(), CreatePasswordActivity.class );
                intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK );
                startActivity( intent );
            } else if (getUserName.equals( user_name ) && getPassword.equals( passworD )) {
                Log.e( TAG, passworD + "database" );
                sharedPreferences = getActivity().getSharedPreferences( "MYPREFERENCES", Context.MODE_PRIVATE );
                editor = sharedPreferences.edit();

                editor.putBoolean( "islogin", true );
                editor.putString( "name", usertext );
                editor.putString( "password", passwordEditText.getText().toString() );
                editor.putString( "memberid", memberId );
                editor.putString( "companyid", companyId );
                editor.apply();

                MainActivity.mIsUserLogIn = true;
                MainActivity mActivity = (MainActivity) getActivity();
                mActivity.switchToSignInMode( memberId );

            } else if (!getUserName.equals( user_name ) || !getPassword.equals( passworD )) {
                progressDialog.dismiss();
                open();
            }

        } else {

            open();
        }
    }

    public void open() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder( getContext() );
        alertDialogBuilder.setMessage( R.string.invalid );
        alertDialogBuilder.setPositiveButton( R.string.btn_ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        clearData();
                    }
                } );

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void clearData() {
        usernameEditText.setError( "" );
        getPassword = "";
        passwordEditText.setError( "" );
        usernameEditText.setText( "" );

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService( Context.CONNECTIVITY_SERVICE );
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
