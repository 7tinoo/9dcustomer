package com.interwebbingtechonolgy.nine_dragons_customer.Fragments;

import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.interwebbingtechonolgy.android_commons_commerce.event.ShowVouncherReceivedEvent;
import com.interwebbingtechonolgy.android_commons_commerce.manager.ShowVouncherDetailManager;
import com.interwebbingtechonolgy.android_commons_commerce.manager.ShowVouncherListManager;
import com.interwebbingtechonolgy.android_commons_commerce.model.ShowVouncherResponse;
import com.interwebbingtechonolgy.nine_dragons_customer.Adapter.Item;
import com.interwebbingtechonolgy.nine_dragons_customer.Adapter.VouncherListAdapter;
import com.interwebbingtechonolgy.nine_dragons_customer.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import de.greenrobot.event.EventBus;


public class VouncherListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    ArrayList<ShowVouncherResponse> transactionResponseArrayList = new ArrayList<>();
    EditText search;
    RecyclerView recyclerView;

    SwipeRefreshLayout swipeRefreshLayout;
    VouncherListAdapter adapter;
    ArrayList<ShowVouncherResponse> allData = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate( R.layout.fragment_vouncher_list, container, false );
        recyclerView = view.findViewById( R.id.member_recycler );
        search = view.findViewById( R.id.search );
        swipeRefreshLayout = view.findViewById( R.id.refreshId );
        swipeRefreshLayout.setOnRefreshListener( this );

        if (!EventBus.getDefault().isRegistered( this )) {
            EventBus.getDefault().register( this );
        }

        SignInFragment.companyId = EmailLoginFragment.companyId;
        SignInFragment.getUserName = EmailLoginFragment.nrc;

        ShowVouncherDetailManager.getInstance().fetchAllVouncherList( SignInFragment.companyId, SignInFragment.getUserName );

        Log.d( "iddddd", String.valueOf( SignInFragment.companyId ) );
        ShowVouncherListManager.getInstance().fetchAllVouncherList( SignInFragment.companyId, SignInFragment.getUserName );

        search.addTextChangedListener( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                adapter.filter( search.getText().toString() );
            }
        } );

        return view;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onEvent(ShowVouncherReceivedEvent showVouncherReceivedEvent) {


        List<ShowVouncherResponse> showVouncherDetailResponseList = showVouncherReceivedEvent.getShowVouncherResponse();

        if (showVouncherDetailResponseList.size() == 0) {

        } else {

            showVouncherList( showVouncherDetailResponseList );

        }

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void showVouncherList(final List<ShowVouncherResponse> deliverDataList) {
        final List<Item> memberArrayList = new ArrayList();
        if (deliverDataList.size() != 0) {

            for (int i = 0; i < deliverDataList.size(); i++) {

                final ShowVouncherResponse allDeliverData = deliverDataList.get( i );
                allData.add( deliverDataList.get( i ) );
                memberArrayList.add( new Item( allDeliverData ) );

            }
            if (getActivity() != null) {
                swipeRefreshLayout.setRefreshing( false );
                adapter = new VouncherListAdapter( memberArrayList, getActivity() );
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager( getActivity(), LinearLayoutManager.VERTICAL, false );
                recyclerView.setLayoutManager( mLayoutManager );
                recyclerView.setItemAnimator( new DefaultItemAnimator() );
                recyclerView.setAdapter( adapter );
                // textView.setVisibility( View.GONE );

            }

        }

    }


    @Override
    public void onResume() {
        super.onResume();
        ShowVouncherListManager.getInstance().fetchAllVouncherList( SignInFragment.companyId, SignInFragment.getUserName );

    }

    @Override
    public void onRefresh() {

        ShowVouncherListManager.getInstance().fetchAllVouncherList( SignInFragment.companyId, SignInFragment.getUserName );

    }
}
