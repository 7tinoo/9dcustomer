package com.interwebbingtechonolgy.nine_dragons_customer.Fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.interwebbingtechonolgy.android_commons_commerce.event.SignInReceivedEvent;
import com.interwebbingtechonolgy.android_commons_commerce.manager.SignInWithEmailManager;
import com.interwebbingtechonolgy.android_commons_commerce.model.MemberData;
import com.interwebbingtechonolgy.nine_dragons_customer.MainActivity;
import com.interwebbingtechonolgy.nine_dragons_customer.R;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import de.greenrobot.event.EventBus;

import static com.interwebbingtechonolgy.android_commons_commerce.preferences.SharepreferenceLoginCode.LOGGED_IN_PREF;


public class EmailLoginFragment extends Fragment {


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    EditText usernameEd;
    EditText passwordEd;
    TextView message;
    Button loginButton, fbButton, googleButton;
    TextView forgetPwd, singUp, showpw;
    List<MemberData> list;
    private MemberData memberData;
    public static String memberId, companyId, email, passworD, nrc;

    private static java.util.regex.Pattern EMAIL_PATTERN = java.util.regex.Pattern.compile( "([a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}[\\@]{1}[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}([\\.]{1}[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+)|(^[0-9]{6}$)" );

    ProgressDialog progressDialog;
    public static String getUserName, getPassword;
    String myprefs = "MyPrefs";
    private MemberData signInModel;
    SharedPreferences sharedPrefs;
    SharedPreferences.Editor editor;
    public static boolean isSuccess;
    public static String sellerId = null;
    public static String user_name = null;
    public static String password = null;
    public static String phone = null;
    public static String name = null;
    public static String photo = null;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        EventBus.getDefault().register( this );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate( R.layout.fragment_email_login, container, false );


        return v;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated( savedInstanceState );

        View v = getView();
        usernameEd = v.findViewById( R.id.edit_username );
        passwordEd = v.findViewById( R.id.edit_password );
        loginButton = v.findViewById( R.id.btn_login );
        showpw = v.findViewById( R.id.tv_showePwd );
        message = v.findViewById( R.id.msg );


        sharedPrefs = getActivity().getSharedPreferences( LOGGED_IN_PREF, Context.MODE_PRIVATE );
        loginButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                signIn();
            }

        } );


        passwordEd.setInputType( InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD );
        passwordEd.addTextChangedListener( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (passwordEd.getText().length() > 0) {
                    showpw.setVisibility( View.VISIBLE );
                } else if (passwordEd.getText().length() == 0) {
                    showpw.setVisibility( View.GONE );
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (passwordEd.getText().length() > 0) {
                    showpw.setVisibility( View.VISIBLE );
                } else if (passwordEd.getText().length() == 0) {
                    showpw.setVisibility( View.GONE );
                }
            }
        } );


        passwordEd.setOnFocusChangeListener( new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                String getNrc = usernameEd.getText().toString();
                if (!isNrcValid( getNrc )) {
                    usernameEd.setError( getString( R.string.validation_email_format ) );
                    usernameEd.requestFocus();
                }
            }
        } );


        passwordEd.setOnKeyListener( new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN &&
                        keyCode == KeyEvent.KEYCODE_ENTER) {
                    SignInWithEmailManager.getInstance().isLogin( getUserName, getPassword );
                    return true;
                }
                return false;
            }
        } );


        showpw.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showpw.getText().equals( "SHOW" )) {
                    showpw.setText( "HIDE" );
                    passwordEd.setInputType( InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD );
                    passwordEd.setSelection( passwordEd.length() );
                } else {
                    showpw.setText( "SHOW" );
                    passwordEd.setInputType( InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD );
                    passwordEd.setSelection( passwordEd.length() );
                }
            }
        } );


    }

    boolean isNrcValid(CharSequence nrc) {
        if (!TextUtils.isEmpty( nrc )) {
            return EMAIL_PATTERN.matcher( nrc ).matches();
        }
        return false;
    }


    private void signIn() {
        getUserName = usernameEd.getText().toString();
        getPassword = passwordEd.getText().toString();

        if (progressDialog == null) {
            progressDialog = new ProgressDialog( getContext() );
            progressDialog.setTitle( R.string.login_title );
            progressDialog.setMessage( getResources().getString( R.string.login_body ) );
            progressDialog.show();
        }
        if (usernameEd.getText().toString().equals( "" ) || passwordEd.getText().toString().equals( "" )) {


            Toast.makeText( getContext(), "အကောင့်အမည် (သို့ ) လျို့ဝှက်နံပါတ်ရိုက်ထည့်ပါ။", Toast.LENGTH_LONG ).show();
            progressDialog.dismiss();

        } else {
            SignInWithEmailManager.getInstance().isLogin( getUserName, getPassword );
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onEvent(SignInReceivedEvent signInReceivedEvent) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        memberData = signInReceivedEvent.getMemberData();
        if (signInReceivedEvent.getMemberData() != null && signInReceivedEvent.isSuccess()) {
            email = memberData.getMemberEmail();
            passworD = memberData.getMemberPassword();
            memberId = memberData.getMemberId();
            name = memberData.getMemberName();
            companyId = memberData.getCompanyId();
            nrc = memberData.getMemberNrc();

            Log.d( "nnnnnnnn", String.valueOf( nrc ) );


            if (getUserName.equals( email ) && getPassword.equals( passworD )) {
                //Log.e( TAG, passworD + "database" );
                sharedPrefs = getActivity().getSharedPreferences( "MYPREFERENCES", Context.MODE_PRIVATE );
                editor = sharedPrefs.edit();

                editor.putBoolean( "islogin", true );
                editor.putString( "ename", email );
                editor.putString( "epassword", passworD );
                editor.putString( "ememberid", memberId );
                editor.putString( "ecompanyid", companyId );
                editor.putString( "enrc", nrc );
                editor.apply();

                MainActivity.mIsUserLogIn = true;
                MainActivity mActivity = (MainActivity) getActivity();
                mActivity.switchToSignInMode( memberId );

            } else if (!getUserName.equals( user_name ) || !getPassword.equals( passworD )) {
                progressDialog.dismiss();
                open();
            }

        } else {

            open();
        }
    }

    public void open() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder( getContext() );
        alertDialogBuilder.setTitle( R.string.invalid );
        alertDialogBuilder.setMessage( R.string.eorpwrong );
        alertDialogBuilder.setPositiveButton( R.string.btn_ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        clearData();
                    }
                } );

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void clearData() {
        usernameEd.setError( "" );
        getPassword = "";
        passwordEd.setError( "" );


    }

}
