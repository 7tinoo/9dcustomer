package com.interwebbingtechonolgy.nine_dragons_customer.utils.encryption;

import java.nio.charset.Charset;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * String encryption class to save the encrypted user preferences, based heavily on
 * the blog post here: https://trivedihardik.wordpress.com/tag/android-aes-example/
 * Switched the key generation method in the original post from SecureRandom to SecretKey
 * because of the blog post here:
 * http://android-developers.blogspot.com/2013/02/using-cryptography-to-store-credentials.html
 * SecureRandom is just not that safe to use any more.
 */
public class StringEncryptor
{
    // Generate a 256-bit key. We could use 128 also, this is just more secure.
    final static int outputKeyLength = 256;
    static byte[] rawKey = new byte[outputKeyLength];

    public StringEncryptor(String password, String salt)
    {
        rawKey = getRawKey(password.toCharArray(), salt.getBytes(Charset.forName("UTF-8")));
    }

    /**
     * Method open for a consumer to use to encrypt a string
     * @param cleartext - text to encrypt
     * @return encrypted string
     * @throws Exception
     */
    public static String encrypt(String cleartext) throws Exception {
        byte[] result = encrypt(rawKey, cleartext.getBytes(Charset.forName("UTF-8")));
        return toHex(result);
    }

    /**
     * Method open for a consumer to use to decrypt a previously encrypted string
     * @param encrypted - encrypted string
     * @return original string
     * @throws Exception
     */
    public static String decrypt(String encrypted) throws Exception {
        byte[] enc = toByte(encrypted);
        byte[] result = decrypt(rawKey, enc);
        return new String(result, Charset.forName("UTF-8"));
    }

    /**
     * Generating a raw key based on the example given here:
     * http://android-developers.blogspot.com/2013/02/using-cryptography-to-store-credentials.html
     * This is one of the safest ways to generate a random secure key
     * @param passphraseOrPin
     * @param salt
     * @return
     */
    private static byte[] getRawKey(char[] passphraseOrPin, byte[] salt)
    {
        byte[] keyGen = new byte[outputKeyLength];
        // Number of PBKDF2 hardening rounds to use. Larger values increase
        // computation time. You should select a value that causes computation
        // to take >100ms.
        final int iterations = 1000;
        try
        {
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            KeySpec keySpec = new PBEKeySpec(passphraseOrPin, salt, iterations, outputKeyLength);
            SecretKey secretKey = secretKeyFactory.generateSecret(keySpec);
            return secretKey.getEncoded();
        } catch (Exception e)
        {
            return keyGen;
        }
    }

    /**
     * Private method to actually perform the encryption
     * @param raw - raw secret key
     * @param clear - text to be encrypted in bytes form
     * @return encrypted text in bytes
     * @throws Exception
     */
    private static byte[] encrypt(byte[] raw, byte[] clear) throws Exception
    {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(clear);
        return encrypted;
    }

    /**
     * Private method to actually perform the decryption
     * @param raw - raw secret key
     * @param encrypted - encrypted text in bytes
     * @return original text in bytes
     * @throws Exception
     */
    private static byte[] decrypt(byte[] raw, byte[] encrypted) throws Exception
    {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] decrypted = cipher.doFinal(encrypted);
        return decrypted;
    }

    //Method to convert the encrypted string into bytes (one character at a time)
    public static byte[] toByte(String hexString)
    {
        int len = hexString.length() / 2;
        byte[] result = new byte[len];
        for (int i = 0; i < len; i++)
            result[i] = Integer.valueOf(hexString.substring(2 * i, 2 * i + 2),
                    16).byteValue();
        return result;
    }

    //Method to convert the bytes into encrypted string (one byte at a time)
    public static String toHex(byte[] buf)
    {
        if (buf == null)
            return "";
        StringBuffer result = new StringBuffer(2 * buf.length);
        for (int i = 0; i < buf.length; i++)
        {
            appendHex(result, buf[i]);
        }
        return result.toString();
    }

    private final static String HEX = "0123456789ABCDEF";

    private static void appendHex(StringBuffer sb, byte b)
    {
        sb.append(HEX.charAt((b >> 4) & 0x0f)).append(HEX.charAt(b & 0x0f));
    }

}
