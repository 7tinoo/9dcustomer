package com.interwebbingtechonolgy.nine_dragons_customer.utils.support;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

/**
 * Created by smart04 on 6/2/17.
 */

public class SystemUtils
{

    /**
     * Determines if the device currently has a usable network connection
     */
    public static boolean isOnline(Context context)
    {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting())
        {
            return true;
        }
        return false;
    }

    /**
     * Determines if the device has Cellular capability.
     */
    public static boolean hasCelluar(Context context)
    {
        TelephonyManager tm = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);

        boolean result = (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_NONE);
        return result;
    }
    /**
     * Determines the cellular service provider's name if available
     */
    public static String getCellularServiceProvider(Context context)
    {
        if (!hasCelluar(context))
            return "";
        else
        {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            String provider = tm.getNetworkOperatorName();
            return provider;
        }
    }

}