package com.interwebbingtechonolgy.nine_dragons_customer.Common;

/**
 * Created by smart04 on 6/12/17.
 */

public class AppConfigBase {
    public static final String DEV_CONFIG = "dev";
    public static final String TEST_CONFIG = "test";
    public static final String STAGE_CONFIG = "stage";
    public static final String BETA_CONFIG = "beta";
    public static final String PROD_CONFIG = "prod";
}
