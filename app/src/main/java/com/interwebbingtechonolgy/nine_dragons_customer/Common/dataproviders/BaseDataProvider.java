package com.interwebbingtechonolgy.nine_dragons_customer.Common.dataproviders;

import android.app.LoaderManager;
import android.content.Context;
import android.graphics.Bitmap;

/**
 * Created by zLandon.
 */
public abstract class BaseDataProvider
{
    /**
     * Default/generic data identifier value.  Used when only one productType of data is being loaded by a given provider.
     * Subclasses should create their own data ids to distinguish between multiple types of data being loaded by the same
     * data provider.
     */
    public static final int GENERIC_DATA_ID = -1;

    protected Context mContext;
    protected LoaderManager mLoaderManager;
    protected Delegate mDelegate;

    public BaseDataProvider(Context context)
    {
        mContext = context;
    }

    public void setDelegate(Delegate delegate)
    {
        mDelegate = delegate;
    }

    public LoaderManager getLoaderManager()
    {
        return mLoaderManager;
    }

    public void setLoaderManager(LoaderManager loaderManager)
    {
        mLoaderManager = loaderManager;
    }

    /**
     * Tells the data provider to start loading data specified by the generic data identifier value.
     *
     * @param forceRefresh True if the data should be forceably refreshed, false otherwise.
     */
    public void loadData(boolean forceRefresh)
    {
        loadData(forceRefresh, GENERIC_DATA_ID);
    }

    /**
     * Destroys the GENERIC_DATA_ID loader, if it has been used/created.
     */
    public void destroyLoader()
    {
        destroyLoader(GENERIC_DATA_ID);
    }

    /**
     * Destroys the loader specified by the passed in loaderId value, if it has been used/created.
     *
     * @param loaderId The id of the loader to destroy.
     */
    public void destroyLoader(int loaderId)
    {
        if (mLoaderManager != null)
            mLoaderManager.destroyLoader(loaderId);
    }

    /**
     * Tells the data provider to start loading data specified by the data identifier value.
     *
     * @param forceRefresh True if the data should be forceably refreshed, false otherwise.
     * @param id           The data identifier value, used to distinguish which productType of data to load.
     */
    public abstract void loadData(boolean forceRefresh, int id);

    public interface Delegate
    {
        void onBackPressed();

        void onImageLoaded(Bitmap bitmap);

        void onError();

        /**
         * Notifies that the loading of the particular data has been completed.
         *
         * @param dataProvider The data provider that has finished loading data.
         * @param id           The data identifier that has finished loading.
         */
        void onLoadComplete(BaseDataProvider dataProvider, int id);
    }
}
