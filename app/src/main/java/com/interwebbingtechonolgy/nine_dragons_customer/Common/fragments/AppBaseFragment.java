package com.interwebbingtechonolgy.nine_dragons_customer.Common.fragments;

/**
 * Created by smart05 on 6/12/17.
 */

import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import java.lang.reflect.Field;

public abstract class AppBaseFragment extends Fragment
{
    protected final String TAG = getClass().getSimpleName();

    //getting a reference to that field and setting it to null when detaching this fragment. only happens on Android 4.4 and below
    // all the code below is to resolve a known android bug for placing view pager with child fragment manager
    // see details at: http://stackoverflow.com/questions/14929907/causing-a-java-illegalstateexception-error-no-activity-only-when-navigating-to/16602540#16602540
    private static final Field sChildFragmentManagerField;

    static
    {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
        {
            Field f = null;
            try
            {
                f = Fragment.class.getDeclaredField("mChildFragmentManager");
                f.setAccessible(true);
            } catch (NoSuchFieldException e)
            {
                Log.d("fragmentProblem", "Error getting mChildFragmentManager field");
            }
            sChildFragmentManagerField = f;
        } else
            sChildFragmentManagerField = null;
    }

    private Bundle bundle;

    @Override
    public void onDetach()
    {
        super.onDetach();

        if (sChildFragmentManagerField != null)
        {
            try
            {
                sChildFragmentManagerField.set(this, null);
            } catch (Exception e)
            {
                Log.d("fragmentProblem", "Error setting mChildFragmentManager field");
            }
        }
    }

    public void updateArgument(Bundle bundle)
    {
        this.bundle = bundle;
    }

    public Bundle getBundle()
    {
        if (bundle != null)
            return bundle;
        else return getArguments();
    }


}