package com.interwebbingtechonolgy.nine_dragons_customer.Activity;

import android.os.Bundle;

import com.interwebbingtechonolgy.nine_dragons_customer.Fragments.BuyFragment;
import com.interwebbingtechonolgy.nine_dragons_customer.Fragments.SignInFragment;
import com.interwebbingtechonolgy.nine_dragons_customer.Fragments.VouncherListFragment;
import com.interwebbingtechonolgy.nine_dragons_customer.R;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

public class BuyandSellActivity extends AppCompatActivity {

    String type;
    String loginS;
    String loginE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_buyand_sell );

        type = getIntent().getExtras().getString( "type" );
        loginS = getIntent().getExtras().getString( "loginS" );
        loginE = getIntent().getExtras().getString( "loginE" );

        if (type.equals( "buy" )) {
            loadBuyFragment();

        } else if (type.equals( "sell" )) {


                loadSellFragment();


        } else if (type.equals( "vouncherList" )) {
            loadVouncherListFragment();
        }
    }


    private void loadBuyFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace( R.id.frag, new BuyFragment() ).commit();
    }

    private void loadSellFragment() {


        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace( R.id.frag, new SignInFragment() ).commit();


    }


    private void loadVouncherListFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace( R.id.frag, new VouncherListFragment() ).commit();
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
