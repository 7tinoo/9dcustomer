package com.interwebbingtechonolgy.nine_dragons_customer.Activity;

import androidx.appcompat.app.AppCompatActivity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.interwebbingtechonolgy.android_commons_commerce.event.CreatePwdReceivedEvent;
import com.interwebbingtechonolgy.android_commons_commerce.manager.SignInManager;
import com.interwebbingtechonolgy.nine_dragons_customer.Fragments.SignInFragment;
import com.interwebbingtechonolgy.nine_dragons_customer.MainActivity;
import com.interwebbingtechonolgy.nine_dragons_customer.R;

import de.greenrobot.event.EventBus;

public class CreatePasswordActivity extends AppCompatActivity {

    private EditText createPwdEditText,confirmPwdEditText;
    private Button createPwdButton;
    private TextView errorTextView,showCreatePwdTextView,showConfirmPwdTextView,usernameTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_password);

        createPwdEditText = findViewById(R.id.edit_createPwd);
        confirmPwdEditText = findViewById(R.id.edit_confirmPwd);
        createPwdButton = findViewById(R.id.btncreate);
        errorTextView = findViewById(R.id.tv_error);
        showCreatePwdTextView = findViewById(R.id.tv_showCreatePwd);
        showConfirmPwdTextView = findViewById(R.id.tv_showConfirmPwd);
        usernameTextView = findViewById(R.id.tv_username);

        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }

        usernameTextView.setText(SignInFragment.getUserName);
        createPwdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!createPwdEditText.getText().toString().equals(confirmPwdEditText.getText().toString())){
                    errorTextView.setVisibility(View.VISIBLE);
                    errorTextView.setText("Mismatch Password");
                }
                else {
                    errorTextView.setVisibility(View.GONE);
                    createNewPassword();
                }

            }
        });

        createPwdEditText.setInputType(InputType.TYPE_CLASS_TEXT |InputType.TYPE_TEXT_VARIATION_PASSWORD);
        confirmPwdEditText.setInputType(InputType.TYPE_CLASS_TEXT |InputType.TYPE_TEXT_VARIATION_PASSWORD);

        createPwdEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if(createPwdEditText.getText().length() > 0){
                    showCreatePwdTextView.setVisibility(View.VISIBLE);
                }
                else {
                    showCreatePwdTextView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(createPwdEditText.getText().length() > 0){
                    showCreatePwdTextView.setVisibility(View.VISIBLE);
                }
                else {
                    showCreatePwdTextView.setVisibility(View.GONE);
                }
            }
        });

        showCreatePwdTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(showCreatePwdTextView.getText().toString().equals("SHOW")){
                    showCreatePwdTextView.setText("HIDE");
                    createPwdEditText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    createPwdEditText.setSelection(createPwdEditText.length());
                }
                else {
                    showCreatePwdTextView.setText("SHOW");
                    createPwdEditText.setInputType(InputType.TYPE_CLASS_TEXT |InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    createPwdEditText.setSelection(createPwdEditText.length());
                }
            }
        });

        confirmPwdEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if(confirmPwdEditText.getText().length() > 0){
                    showConfirmPwdTextView.setVisibility(View.VISIBLE);
                }
                else {
                    showConfirmPwdTextView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(confirmPwdEditText.getText().length() > 0){
                    showConfirmPwdTextView.setVisibility(View.VISIBLE);
                }
                else {
                    showConfirmPwdTextView.setVisibility(View.GONE);
                }
            }
        });

        showConfirmPwdTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(showConfirmPwdTextView.getText().toString().equals("SHOW")){
                    showConfirmPwdTextView.setText("HIDE");
                    confirmPwdEditText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    confirmPwdEditText.setSelection(confirmPwdEditText.length());
                }
                else {
                    showConfirmPwdTextView.setText("SHOW");
                    confirmPwdEditText.setInputType(InputType.TYPE_CLASS_TEXT |InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    confirmPwdEditText.setSelection(confirmPwdEditText.length());
                }
            }
        });
    }

    private void createNewPassword() {
        if(!confirmPwdEditText.getText().toString().equals("9dpass")){
            SignInManager.getInstance().createMemberPwd(SignInFragment.getUserName,confirmPwdEditText.getText().toString());
        }
    }

    public void onEvent(CreatePwdReceivedEvent createPwdReceivedEvent){
        if(createPwdReceivedEvent.isSuccess() == false){
           open();
        }
    }

    public void open() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CreatePasswordActivity.this);
        alertDialogBuilder.setMessage( R.string.create_success );
        alertDialogBuilder.setPositiveButton( R.string.btn_ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                        startActivity(new Intent(CreatePasswordActivity.this, MainActivity.class));
                    }
                } );

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
