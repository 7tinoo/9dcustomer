package com.interwebbingtechonolgy.nine_dragons_customer.Activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.interwebbingtechonolgy.android_commons_commerce.model.ShowVouncherDetailResponse;
import com.interwebbingtechonolgy.nine_dragons_customer.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class VouncherDetailActivity extends AppCompatActivity {
    ImageView imageView;
    TextView name, weight, price, qty, code, promo;
    String get_phone, get_name, get_weight, get_price, get_qty, get_image, get_code, get_promo;

    List<ShowVouncherDetailResponse> memberData;

    CollapsingToolbarLayout collapsingToolbar;
    String image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_vouncher_detail );

        imageView = findViewById( R.id.image );
        name = findViewById( R.id.item_name );
        weight = findViewById( R.id.item_weight );
        price = findViewById( R.id.item_price );
        qty = findViewById( R.id.item_qty );
        code = findViewById( R.id.item_code );
        promo = findViewById( R.id.promo );

        final Toolbar toolbar = (Toolbar) findViewById( R.id.tool );
        setSupportActionBar( toolbar );
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        toolbar.setNavigationOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        } );

        collapsingToolbar = findViewById( R.id.coll );
        collapsingToolbar.setTitle( get_name );
        collapsingToolbar.setExpandedTitleColor( getResources().getColor( R.color.white ) );
        collapsingToolbar.setCollapsedTitleTypeface( Typeface.DEFAULT );

        get_name = getIntent().getExtras().getString( "name" );
        get_promo = getIntent().getExtras().getString( "prom" );
        get_price = getIntent().getExtras().getString( "price" );
        get_weight = getIntent().getExtras().getString( "weight" );
        get_qty = getIntent().getExtras().getString( "qty" );
        get_image = getIntent().getExtras().getString( "image" );
        get_code = getIntent().getExtras().getString( "code" );
        // = getIntent().getExtras().getString("member_nrc");


        name.setText( get_name );
        code.setText( get_code );
        weight.setText( get_weight );
        price.setText( get_price + " ကျပ်" );
        qty.setText( get_qty  + " ပဲရည်");
        promo.setText( get_promo );
        Picasso.with( getApplication() )
                .load( get_image )
                .placeholder( R.drawable.ic_launcher_background )
                .into( imageView );
    }
}
