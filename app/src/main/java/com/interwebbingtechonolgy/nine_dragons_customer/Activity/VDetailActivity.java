package com.interwebbingtechonolgy.nine_dragons_customer.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.interwebbingtechonolgy.android_commons_commerce.model.ShowVouncherResponse;
import com.interwebbingtechonolgy.android_commons_commerce.model.VouncherData;
import com.interwebbingtechonolgy.android_commons_commerce.model.VouncherList;
import com.interwebbingtechonolgy.nine_dragons_customer.Adapter.VouncherAdapter;
import com.interwebbingtechonolgy.nine_dragons_customer.R;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;

public class VDetailActivity extends AppCompatActivity {

    ListView cartListView;
    ShowVouncherResponse showVouncherResponse;
    String id, date;
    TextView tvId, tvDate;

    VouncherAdapter mAdapter;
    public static ArrayList<VouncherData> cartList = new ArrayList<>();
    ArrayList<VouncherList> vouncherLists = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_vdetail );
        tvDate = findViewById( R.id.tttdate );
        tvId = findViewById( R.id.ttvid );

        Intent in = getIntent();
        showVouncherResponse = in.getParcelableExtra( "id" );
        id = in.getStringExtra( "helloid" );
        date = in.getStringExtra( "date" );

        String[] date2 = date.split( "T" );
        tvDate.setText( ":  " + date2[0] );
        tvId.setText( ":  " + id );
        //    Log.d( "llllllll", String.valueOf( id ) );


        cartListView = (ListView) findViewById( R.id.cart_vouncher_list );
        cartList.clear();
        for (int i = 0; i < showVouncherResponse.getVouncherList().length; i++) {

            vouncherLists.add( showVouncherResponse.getVouncherList()[i] );

            VouncherData vouncherData = new VouncherData( vouncherLists );

            cartList.add( vouncherData );
        }
        mAdapter = new VouncherAdapter( this, R.layout.cart_list_item, cartList );
        cartListView.setAdapter( mAdapter );
    }
}
