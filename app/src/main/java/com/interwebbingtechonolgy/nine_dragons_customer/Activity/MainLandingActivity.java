package com.interwebbingtechonolgy.nine_dragons_customer.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.interwebbingtechonolgy.nine_dragons_customer.MainActivity;
import com.interwebbingtechonolgy.nine_dragons_customer.R;

import androidx.appcompat.app.AppCompatActivity;

public class MainLandingActivity extends AppCompatActivity {
    RelativeLayout buyLayout, sellLayout, vouncherList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_mainlanding );
        buyLayout = (RelativeLayout) findViewById( R.id.buy_layout );
        sellLayout = (RelativeLayout) findViewById( R.id.sell_layout );
        vouncherList = (RelativeLayout) findViewById( R.id.vouncher_layout );


        buyLayout.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent( MainLandingActivity.this, BuyandSellActivity.class );
                intent.putExtra( "type", "buy" );
                startActivity( intent );
            }
        } );

        sellLayout.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( MainLandingActivity.this, BuyandSellActivity.class );
                intent.putExtra( "type", "sell" );


                startActivity( intent );

            }
        } );

        vouncherList.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( MainLandingActivity.this, BuyandSellActivity.class );
                intent.putExtra( "type", "vouncherList" );
                startActivity( intent );
            }
        } );

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate( R.menu.menubar, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_logout: {
                logout();
                return true;
            }

            case R.id.menu_profile: {
                seeProfile();


                return true;
            }
        }
        return super.onOptionsItemSelected( item );
    }

    private void logout() {
        final Dialog dialog = new Dialog( this );
        dialog.requestWindowFeature( Window.FEATURE_NO_TITLE );
        dialog.setContentView( R.layout.logout_dialog );
        dialog.setCanceledOnTouchOutside( false );

        Button dialog_cancel = (Button) dialog.findViewById( R.id.btncancel );
        Button dialog_confirm = (Button) dialog.findViewById( R.id.btnconfirm );

        dialog_cancel.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        } );

        dialog_confirm.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                SharedPreferences sharedPrefs = getSharedPreferences( "MYPREFERENCES", Context.MODE_PRIVATE );
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putBoolean( "islogin", false );
                editor.apply();
                Intent intent = new Intent( getApplication(), MainActivity.class );
                startActivity( intent );
            }
        } );

        dialog.show();
    }


    private void seeProfile() {
        Toast.makeText( getApplicationContext(), "profile", Toast.LENGTH_LONG ).show();
        Intent intent = new Intent( getApplicationContext(), ProfileActivity.class );
        startActivity( intent );

    }
}
