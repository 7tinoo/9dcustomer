package com.interwebbingtechonolgy.nine_dragons_customer.Activity;

import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.interwebbingtechonolgy.android_commons_commerce.event.ProfileReceivedEvent;
import com.interwebbingtechonolgy.android_commons_commerce.manager.ProfileManager;
import com.interwebbingtechonolgy.android_commons_commerce.model.Member;
import com.interwebbingtechonolgy.nine_dragons_customer.Fragments.EmailLoginFragment;
import com.interwebbingtechonolgy.nine_dragons_customer.Fragments.SignInFragment;
import com.interwebbingtechonolgy.nine_dragons_customer.R;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import de.greenrobot.event.EventBus;

public class ProfileActivity extends AppCompatActivity {

    TextView email;
    TextView name;
    TextView gender;
    TextView id;
    TextView phone;
    TextView roleCategory;
    ImageView pfImage;
    public static Member allUserResponses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_profile );

        if (!EventBus.getDefault().isRegistered( this )) {
            EventBus.getDefault().register( this );
        }
        name = findViewById( R.id.name );
        email = findViewById( R.id.email );
        pfImage = findViewById( R.id.img_profile );
        roleCategory = findViewById( R.id.roleCategory );
        gender = findViewById( R.id.gender );
        SignInFragment.getUserName = EmailLoginFragment.nrc;

        ProfileManager.getInstance().fetchAllItemList( SignInFragment.getUserName );


    }

    @RequiresApi(api = Build.VERSION_CODES.M)

    public void onEvent(ProfileReceivedEvent profileReceivedEvent) {

        allUserResponses = profileReceivedEvent.getUserModel();
        // Log.d( "meeee", String.valueOf( allUserResponses ) );
        name.setText( allUserResponses.getMemberName() );
        email.setText( allUserResponses.getMemberNRC() );
        roleCategory.setText( allUserResponses.getMemberLevel() );
        // phone.setText( allUserResponses.getAddress() );
        gender.setText( allUserResponses.getAddress() );

    }


}
