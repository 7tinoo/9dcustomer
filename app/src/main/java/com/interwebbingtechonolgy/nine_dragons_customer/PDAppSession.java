package com.interwebbingtechonolgy.nine_dragons_customer;

import android.app.Activity;
import android.content.Context;

import com.interwebbingtechonolgy.android_commons_commerce.manager.ProfileManager;
import com.interwebbingtechonolgy.android_commons_commerce.manager.ShowVouncherDetailManager;
import com.interwebbingtechonolgy.android_commons_commerce.manager.ShowVouncherListManager;
import com.interwebbingtechonolgy.android_commons_commerce.manager.SignInManager;
import com.interwebbingtechonolgy.android_commons_commerce.manager.SignInWithEmailManager;
import com.interwebbingtechonolgy.android_commons_commerce.manager.TestingManager;
import com.interwebbingtechonolgy.nine_dragons_customer.app.AppConfig;
import com.interwebbingtechonolgy.nine_dragons_customer.app.Preferences;
import com.interwebbingtechonolgy.nine_dragons_customer.utils.support.SystemUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class PDAppSession extends Activity {

    public static final String SILENT_STORE_LOGIN = "SILENT_STORE_LOGIN";

    private static PDAppSession sInstanceAppSession;
    private boolean mInitialized = false;
    private AppConfig mAppConfig;
    private Context mAppContext;
    private SimpleDateFormat serverDateFormat;
    private boolean isSuccess;

    private PDAppSession(Context appContext, AppConfig appConfig) {
        this.mAppConfig = appConfig;
        this.mAppContext = appContext;

        serverDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);

        Preferences.initPreferences(appContext, appConfig);
        TestingManager.getInstance(appContext);
        SignInManager.getInstance(appContext);
        ShowVouncherDetailManager.getInstance(appContext);
        ShowVouncherListManager.getInstance(appContext);
        ProfileManager.getInstance(appContext);
        SignInWithEmailManager.getInstance(appContext);
        refreshAuthTokenIfNecessary();
    }

    public static PDAppSession getInstance() {
        return sInstanceAppSession;
    }

    public static PDAppSession getInstance(Context appContext, AppConfig appConfig) {
        if (sInstanceAppSession == null) {
            sInstanceAppSession = new PDAppSession(appContext, appConfig);
        }
        return sInstanceAppSession;
    }

    public boolean isInitialized() {
        return mInitialized;
    }

    public void initialize() {
        try {
            if (!refreshAuthTokenIfNecessary()) {
                if (SystemUtils.isOnline(mAppContext)) {
                    // do any pending synchronization
                }
            }
        } catch (Exception err) {

        }
    }

    public boolean refreshAuthTokenIfNecessary() {
        String authTokenExpirationString = Preferences.getEwfAuthTokenExpiration();
        String authTokenString = Preferences.getEwfAuthToken();
        Date authExpirationDate = null;
        Date currentDate = null;
        boolean didRefreshed = false;

        try {
            if (!authTokenString.equals("")) {
                authExpirationDate = serverDateFormat.parse(authTokenExpirationString);
                currentDate = new Date();
            }

            if (currentDate != null) {
                if (currentDate.after(authExpirationDate)) {
                    refreshAuthToken();
                    didRefreshed = true;
                }
            }
        } catch (ParseException e) {
//            EventBus.getDefault().post(new AuthenticationFailedEvent());
            e.printStackTrace();
        } catch (Exception e) {
//            EventBus.getDefault().post(new AuthenticationFailedEvent());
        }
        return didRefreshed;
    }

    public void refreshAuthToken() {
    }

}
