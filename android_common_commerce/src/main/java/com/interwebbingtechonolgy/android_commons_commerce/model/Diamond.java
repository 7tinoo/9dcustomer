package com.interwebbingtechonolgy.android_commons_commerce.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Smart02 on 1/24/18.
 */

public class Diamond implements Parcelable {

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getCut() {
        return cut;
    }

    public void setCut(String cut) {
        this.cut = cut;
    }

    public String getClarity() {
        return clarity;
    }

    public void setClarity(String clarity) {
        this.clarity = clarity;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCarat() {
        return carat;
    }

    public void setCarat(String carat) {
        this.carat = carat;
    }

    public String getRateId() {
        return rateId;
    }

    public void setRateId(String rateId) {
        this.rateId = rateId;
    }

    @SerializedName("rate_id")
    private String rateId;
    private String count;
    private String cut;
    private String color;
    private String clarity;
    private String carat;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(rateId);
        dest.writeString(count);
        dest.writeString(cut);
        dest.writeString(color);
        dest.writeString(clarity);
        dest.writeString(carat);
    }

    public Diamond(Parcel source) {

        setRateId(source.readString());
        setCount(source.readString());
        setCut(source.readString());
        setColor(source.readString());
        setClarity(source.readString());
        setCarat(source.readString());
    }

    public static final Parcelable.Creator<Diamond> CREATOR = new Parcelable.Creator<Diamond>() {
        @Override
        public Diamond createFromParcel(Parcel source) {
            return new Diamond(source);
        }

        @Override
        public Diamond[] newArray(int size) {
            return new Diamond[size];
        }
    };
}
