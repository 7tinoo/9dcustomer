package com.interwebbingtechonolgy.android_commons_commerce.model;

public class CreateMemberPwdResponse {
    boolean isSuccess;

    public CreateMemberPwdResponse(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }
}
