package com.interwebbingtechonolgy.android_commons_commerce.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by smart05 on 6/12/18.
 */

public class VouncherList implements Parcelable {

    @SerializedName("category_id")
    private String categoryId;

    @SerializedName("item_code")
    private String itemCode;

    @SerializedName("item_name")
    private String itemName;

    @SerializedName("item_image")
    private String itemImage;

    @SerializedName("item_owner")
    private String itemOwner;

    public String getFinalGram() {
        return finalGram;
    }

    public void setFinalGram(String finalGram) {
        this.finalGram = finalGram;
    }

    @SerializedName("final_gram")
    private String finalGram;

    @SerializedName("original_item_individual_reduce_value")
    private String originalReduceIndividual;

    private String quality;

    private String gram;

    private Gold[] gold;

    private Diamond[] diamond;

    private Gem[] gem;

    private String promotion;
    private String length;

    public String getReduce() {
        return reduce;
    }

    public void setReduce(String reduce) {
        this.reduce = reduce;
    }

    private String reduce;
    @SerializedName("final_price")
    private String price;


    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    private String totalPrice;

    @SerializedName("external_fee")
    private String externalFee;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public String getItemOwner() {
        return itemOwner;
    }

    public void setItemOwner(String itemOwner) {
        this.itemOwner = itemOwner;
    }
    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }


    public String getOriginalReduceIndividual() {
        return originalReduceIndividual;
    }

    public void setOriginalReduceIndividual(String originalReduceIndividual) {
        this.originalReduceIndividual = originalReduceIndividual;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public String getGram() {
        return gram;
    }

    public void setGram(String gram) {
        this.gram = gram;
    }

    public Gold[] getGold() {
        return gold;
    }

    public void setGold(Gold[] gold) {
        this.gold = gold;
    }

    public Diamond[] getDiamond() {
        return diamond;
    }

    public void setDiamond(Diamond[] diamond) {
        this.diamond = diamond;
    }

    public Gem[] getGem() {
        return gem;
    }

    public void setGem(Gem[] gem) {
        this.gem = gem;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    public String getExternalFee() {
        return externalFee;
    }

    public void setExternalFee(String externalFee) {
        this.externalFee = externalFee;
    }

    public static Creator<VouncherList> getCREATOR() {
        return CREATOR;
    }

    public static final Parcelable.Creator<VouncherList> CREATOR = new Parcelable.Creator<VouncherList>() {
        @Override
        public VouncherList createFromParcel(Parcel source) {
            return new VouncherList(source);
        }

        @Override
        public VouncherList[] newArray(int size) {
            return new VouncherList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(categoryId);
        dest.writeString(itemCode);
        dest.writeString(itemName);
        dest.writeString(itemImage);
        dest.writeString(itemOwner);
        dest.writeString(finalGram);
        dest.writeString(originalReduceIndividual);
        dest.writeString(promotion);
        dest.writeString(price);
        dest.writeString(quality);
        dest.writeString(externalFee);
        dest.writeString(gram);
        dest.writeTypedArray(gold, 0);
        dest.writeTypedArray(diamond, 0);
        dest.writeTypedArray(gem, 0);
        dest.writeString(totalPrice);
        dest.writeString(length);
        dest.writeString(reduce);

    }

    public VouncherList(Parcel source){
        setCategoryId(source.readString());
        setItemCode(source.readString());
        setItemName(source.readString());
        setItemImage(source.readString());
        setItemOwner(source.readString());
        setFinalGram(source.readString());
        setOriginalReduceIndividual(source.readString());
        setPromotion(source.readString());
        setPrice(source.readString());
        setQuality(source.readString());
        setExternalFee(source.readString());
        setGram(source.readString());
        gold = source.createTypedArray(Gold.CREATOR);
        setGold(gold);
        diamond = source.createTypedArray(Diamond.CREATOR);
        setDiamond(diamond);
        gem = source.createTypedArray(Gem.CREATOR);
        setGem(gem);
        setTotalPrice(source.readString());
        setLength(source.readString());
        setReduce(source.readString());

    }
}
