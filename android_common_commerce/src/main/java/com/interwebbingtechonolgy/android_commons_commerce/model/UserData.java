package com.interwebbingtechonolgy.android_commons_commerce.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ko Mg on 1/17/2018.
 */

public class UserData {
    @SerializedName("first_name")
    private String firstname;

    @SerializedName("last_name")
    private String lastname;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String username;
    private String email;
    private String gender;

    @SerializedName("profile_image")
    private String pfImg;

    @SerializedName("role_category")
    private String rolecategory;


    @SerializedName("acc_id")
    private String accId;

    @SerializedName("acc_name")
    private String accName;

    @SerializedName("shop_name")
    private String shopName;

    @SerializedName("company_id")
    private String companyid;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    private String address;

    public String getCompanyid() {
        return companyid;
    }

    public void setCompanyid(String companyid) {
        this.companyid = companyid;
    }

    public String getGender() {
        return gender;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFirstname() {

        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getAccId() {
        return accId;
    }

    public void setAccId(String accId) {
        this.accId = accId;
    }

    public String getAccName() {
        return accName;
    }

    public void setAccName(String accName) {
        this.accName = accName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getPfImg() {
        return pfImg;
    }

    public void setPfImg(String pfImg) {
        this.pfImg = pfImg;
    }

    public String getRolecategory() {
        return rolecategory;
    }

    public void setRolecategory(String rolecategory) {
        this.rolecategory = rolecategory;
    }
}
