package com.interwebbingtechonolgy.android_commons_commerce.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Smart02 on 1/24/18.
 */

public class Gold implements Parcelable {
    public String getGold_gram() {
        return goldGram;
    }

    public void setGold_gram(String goldGram) {
        this.goldGram = goldGram;
    }

    @SerializedName("gold_gram")
    private String goldGram;

    public String getGoldQuality() {
        return goldQuality;
    }

    public void setGoldQuality(String goldQuality) {
        this.goldQuality = goldQuality;
    }

    @SerializedName("gold_quality")
    private String goldQuality;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(goldGram);
        dest.writeString(goldQuality);

    }


    public Gold(Parcel source) {

        setGold_gram(source.readString());
        setGoldQuality(source.readString());
    }

    public static final Parcelable.Creator<Gold> CREATOR = new Parcelable.Creator<Gold>() {
        @Override
        public Gold createFromParcel(Parcel source) {
            return new Gold(source);
        }

        @Override
        public Gold[] newArray(int size) {
            return new Gold[size];
        }
    };
}
