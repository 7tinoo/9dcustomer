package com.interwebbingtechonolgy.android_commons_commerce.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ShowMemberResponse implements Parcelable {

    @SerializedName("member_level")
    private String memberLevel;

    @SerializedName("member_name")
    private String memberName;

    @SerializedName("member_phone")
    private String memberPhone;

    @SerializedName("member_nrc")
    private String memberNrc;

    @SerializedName("member_image")
    private String memberImage;

    @SerializedName("member_address")
    private String address;

    @SerializedName("member_id")
    private String memberid;


    public String getMemberImage() {
        return memberImage;
    }

    public void setMemberImage(String memberImage) {
        this.memberImage = memberImage;
    }

    public String getMemberLevel() {
        return memberLevel;
    }

    public void setMemberLevel(String memberLevel) {
        this.memberLevel = memberLevel;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberPhone() {
        return memberPhone;
    }

    public void setMemberPhone(String memberPhone) {
        this.memberPhone = memberPhone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int describeContents() {
        return 0;
    }

    public String getMemberid() {
        return memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getMemberNrc() {
        return memberNrc;
    }

    public void setMemberNrc(String memberNrc) {
        this.memberNrc = memberNrc;
    }
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(memberLevel);
        dest.writeString(memberName);
        dest.writeString(memberPhone);
        dest.writeString(memberNrc);
        dest.writeString(address);
        dest.writeString(memberid);
        dest.writeString(memberImage);
    }


    public ShowMemberResponse(Parcel source) {
        setMemberLevel(source.readString());
        setMemberName(source.readString());
        setMemberPhone(source.readString());
        setMemberNrc(source.readString());
        setAddress(source.readString());
        setMemberid(source.readString());
        setMemberImage(source.readString());

    }

    public static final Parcelable.Creator<ShowMemberResponse> CREATOR = new Parcelable.Creator<ShowMemberResponse>() {
        @Override
        public ShowMemberResponse createFromParcel(Parcel source) {
            return new ShowMemberResponse(source);
        }

        @Override
        public ShowMemberResponse[] newArray(int size) {
            return new ShowMemberResponse[size];
        }
    };


}
