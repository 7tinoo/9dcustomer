package com.interwebbingtechonolgy.android_commons_commerce.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by smart05 on 6/9/18.
 */

public class ShowVouncherResponse implements Parcelable {

    @SerializedName("company_id")
    private String companyId;

    @SerializedName("vouncher_id")
    private String vouncherId;

    public ShowVouncherResponse() {
    }

    public String getVouncherId() {
        return vouncherId;
    }

    public void setVouncherId(String vouncherId) {
        this.vouncherId = vouncherId;
    }

    private String email;

    private String totalPrice;

    @SerializedName("vouncher_list")
    private  VouncherList[]  vouncherList;

    public VouncherList[] getVouncherList() {
        return vouncherList;
    }

    public void setVouncherList(VouncherList[] vouncherList) {
        this.vouncherList = vouncherList;
    }

    @SerializedName("member_name")
    private String memberName;

    @SerializedName("member_level")
    private String memberLevel;


    @SerializedName("member_phone")
    private String memberPhone;


    @SerializedName("member_nrc")
    private String memberNRC;

    @SerializedName("member_address")
    private String memberAddress;

    @SerializedName("created_at")
    private String createdAt;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public static Creator<ShowVouncherResponse> getCREATOR() {
        return CREATOR;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }


    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberLevel() {
        return memberLevel;
    }

    public void setMemberLevel(String memberLevel) {
        this.memberLevel = memberLevel;
    }

    public String getMemberPhone() {
        return memberPhone;
    }

    public void setMemberPhone(String memberPhone) {
        this.memberPhone = memberPhone;
    }

    public String getMemberNRC() {
        return memberNRC;
    }

    public void setMemberNRC(String memberNRC) {
        this.memberNRC = memberNRC;
    }

    public String getMemberAddress() {
        return memberAddress;
    }

    public void setMemberAddress(String memberAddress) {
        this.memberAddress = memberAddress;
    }

    public String getHighR() {
        return highR;
    }

    public void setHighR(String highR) {
        this.highR = highR;
    }

    public String getMedR() {
        return medR;
    }

    public void setMedR(String medR) {
        this.medR = medR;
    }

    public String getLowR() {
        return lowR;
    }

    public void setLowR(String lowR) {
        this.lowR = lowR;
    }

    @SerializedName("high_gold_rate")
    private String highR;

    @SerializedName("mid_gold_rate")
    private String medR;


    @SerializedName("low_gold_rate")
    private String lowR;


    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(memberLevel);
        dest.writeString(memberName);
        dest.writeString(memberPhone);
        dest.writeString(memberAddress);
        dest.writeString(memberNRC);
        dest.writeString(vouncherId);
        dest.writeString(highR);
        dest.writeString(medR);
        dest.writeString(lowR);
        dest.writeTypedArray(vouncherList, 0);
        dest.writeString(createdAt);
    }


    public ShowVouncherResponse(Parcel source) {
        setMemberLevel(source.readString());
        setMemberName(source.readString());
        setMemberPhone(source.readString());
        setMemberAddress(source.readString());
        setMemberNRC(source.readString());
        setVouncherId(source.readString());
        setHighR(source.readString());
        setMedR(source.readString());
        setLowR(source.readString());
        vouncherList = source.createTypedArray(VouncherList.CREATOR);
        setVouncherList(vouncherList);
        setCreatedAt(source.readString());

    }

    public static final Parcelable.Creator<ShowVouncherResponse> CREATOR = new Parcelable.Creator<ShowVouncherResponse>() {
        @Override
        public ShowVouncherResponse createFromParcel(Parcel source) {
            return new ShowVouncherResponse(source);
        }

        @Override
        public ShowVouncherResponse[] newArray(int size) {
            return new ShowVouncherResponse[size];

        }
    };


}
