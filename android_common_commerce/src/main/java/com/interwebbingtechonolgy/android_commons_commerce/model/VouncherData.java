package com.interwebbingtechonolgy.android_commons_commerce.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class VouncherData implements Parcelable {
    ArrayList<VouncherList> allItemData;




    public VouncherData(ArrayList<VouncherList> allItemData) {
        this.allItemData = allItemData;
    }

    public VouncherData(Parcel in) {
        setAllItemData((ArrayList<VouncherList>) in.readParcelable(VouncherList[].class.getClassLoader()));
    }



    public ArrayList<VouncherList> getAllItemData() {
        return allItemData;
    }

    public void setAllItemData(ArrayList<VouncherList> allItemData) {
        this.allItemData = allItemData;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable((Parcelable) allItemData, flags);
    }

    public static final Creator<VouncherData> CREATOR = new Creator<VouncherData>() {
        @Override
        public VouncherData createFromParcel(Parcel in) {
            return new VouncherData(in);
        }

        @Override
        public VouncherData[] newArray(int size) {
            return new VouncherData[size];
        }
    };
}
