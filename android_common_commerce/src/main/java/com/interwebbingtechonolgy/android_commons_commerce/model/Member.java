package com.interwebbingtechonolgy.android_commons_commerce.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by smart05 on 1/2/18.
 */

public class Member {

    @SerializedName("member_id")
    private String memberId;


    @SerializedName("member_name")
    private String memberName;


    public String getMemberId() {
        return memberId;
    }

    @SerializedName("member_nrc")
    private String memberNRC;

    @SerializedName("member_image")
    private String memberImage;

    @SerializedName("member_phone")
    private String memberPhone;

    @SerializedName("member_level")
    private String memberLevel;



    @SerializedName("member_address")
    private String address;

    @SerializedName("member_email")
    private String email;

    private String createdAt;


    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("company_id")
    private String companyId;

    public Member(String memberId, String memberName, String memberNRC, String memberPhone, String memberLevel, String address, String image, String createdAt, String updatedAt, String cId, String email) {

        this.memberId=memberId;
        this.memberName=memberName;
        this.memberNRC=memberNRC;
        this.memberPhone=memberPhone;
        this.memberLevel=memberLevel;
        this.address=address;
        this.memberImage=image;
        this.createdAt=createdAt;
        this.updatedAt=updatedAt;
        this.companyId=cId;
        this.email=email;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberNRC() {
        return memberNRC;
    }

    public void setMemberNRC(String memberNRC) {
        this.memberNRC = memberNRC;
    }

    public String getMemberPhone() {
        return memberPhone;
    }

    public void setMemberPhone(String memberPhone) {
        this.memberPhone = memberPhone;
    }

    public String getMemberLevel() {
        return memberLevel;
    }

    public void setMemberLevel(String memberLevel) {
        this.memberLevel = memberLevel;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getMemberImage() {
        return memberImage;
    }

    public void setMemberImage(String memberImage) {
        this.memberImage = memberImage;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
