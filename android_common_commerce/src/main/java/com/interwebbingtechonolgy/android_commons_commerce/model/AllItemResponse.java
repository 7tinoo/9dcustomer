package com.interwebbingtechonolgy.android_commons_commerce.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class AllItemResponse implements Parcelable {

    @SerializedName("item_id")
    private String itemId;

    @SerializedName("company_id")
    private String companyId;

    @SerializedName("category_id")
    private String CateId;

    public AllItemResponse() {

    }


    public AllItemResponse getAllItemResponse() {
        return allItemResponse;
    }

    public void setAllItemResponse(AllItemResponse allItemResponse) {
        this.allItemResponse = allItemResponse;
    }

    AllItemResponse allItemResponse;


    public String getCateId() {
        return CateId;
    }

    public void setCateId(String cateId) {
        CateId = cateId;
    }

    @SerializedName("item_code")
    private String itemCode;

    @SerializedName("item_name")
    private String itemName;

    @SerializedName("item_image")
    private String itemImage;

    @SerializedName("item_owner")
    private String itemOwner;

    @SerializedName("item_owner_id")
    private String itemOwnerId;

    @SerializedName("item_original_reduce_value")
    private String itemOriginalReduce;

    @SerializedName("item_wholesale_reduce_value")
    private String itemWholeSaleReduce;

    @SerializedName("item_individual_reduce_value")
    private String itemReduce;

    @SerializedName("item_original_value")
    private String itemOriginalValue;

    private String quality;

    private String gram;

    private String hold;

    @SerializedName("to_owner")
    private String toShop;

    @SerializedName("status")
    private String status;

    public AllItemResponse(String toShop, String status) {
        this.toShop = toShop;
        this.status = status;
    }

    public String getToShop() {
        return toShop;
    }

    public void setToShop(String toShop) {
        this.toShop = toShop;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static Creator<AllItemResponse> getCREATOR() {
        return CREATOR;
    }

    private String cost;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    private String success;

    private String length;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("updated_at")
    private String updatedAt;

    private Gold[] gold;

    private Diamond[] diamond;

    private Gem[] gem;

    public Gold[] getGold() {
        return gold;
    }

    public void setGold(Gold[] gold) {
        this.gold = gold;
    }

    public  Diamond[] getDiamond() {
        return diamond;
    }

    public void setDiamond(Diamond[] diamond) {
        this.diamond = diamond;
    }

    public Gem[] getGem() {
        return gem;
    }

    public void setGem(Gem[] gem) {
        this.gem = gem;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public String getItemOwner() {
        return itemOwner;
    }

    public void setItemOwner(String itemOwner) {
        this.itemOwner = itemOwner;
    }

    public String getItemReduce() {
        return itemReduce;
    }

    public void setItemReduce(String itemReduce) {
        this.itemReduce = itemReduce;
    }

    public String getItemOriginalReduce() {
        return itemOriginalReduce;
    }

    public void setItemOriginalReduce(String itemOriginalReduce) {
        this.itemOriginalReduce = itemOriginalReduce;
    }

    public String getItemWholeSaleReduce() {
        return itemWholeSaleReduce;
    }

    public void setItemWholeSaleReduce(String itemWholeSaleReduce) {
        this.itemWholeSaleReduce = itemWholeSaleReduce;
    }

    public String getItemOriginalValue() {
        return itemOriginalValue;
    }

    public void setItemOriginalValue(String itemOriginalValue) {
        this.itemOriginalValue = itemOriginalValue;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public String getGram() {
        return gram;
    }

    public void setGram(String gram) {
        this.gram = gram;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }


    public String getHold() {
        return hold;
    }

    public void setHold(String hold) {
        this.hold = hold;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getItemOwnerId() {
        return itemOwnerId;
    }

    public void setItemOwnerId(String itemOwnerId) {
        this.itemOwnerId = itemOwnerId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public AllItemResponse(String itemId, String length, String quality, String gram, String cost, String itemOriginalValue, String itemImage, String com_Id ) {
        this.itemId = itemId;
        this.itemOriginalValue = itemOriginalValue;
        this.quality = quality;
        this.gram = gram;
        this.cost = cost;
        this.length = length;
        this.itemImage=itemImage;
        this.companyId=com_Id;

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(itemCode);
        dest.writeString(itemName);
        dest.writeString(itemImage);
        dest.writeString(itemOwner);
        dest.writeString(itemOriginalReduce);
        dest.writeString(itemWholeSaleReduce);
        dest.writeString(itemReduce);
        dest.writeString(itemOriginalValue);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeString(quality);
        dest.writeString(gram);

        dest.writeTypedArray(gold, 0);
        dest.writeTypedArray(diamond, 0);
        dest.writeTypedArray(gem, 0);
        dest.writeString(cost);
        dest.writeString(length);
        dest.writeString(hold);
        dest.writeString(success);
        dest.writeString(companyId);
        dest.writeString(itemOwnerId);
    }


    public AllItemResponse(Parcel source) {
        setItemCode(source.readString());
        setItemName(source.readString());
        setItemImage(source.readString());
        setItemOwner(source.readString());
        setItemOriginalReduce(source.readString());
        setItemWholeSaleReduce(source.readString());
        setItemReduce(source.readString());
        setItemOriginalValue(source.readString());
        setCreatedAt(source.readString());
        setUpdatedAt(source.readString());
        setQuality(source.readString());
        setGram(source.readString());

        gold = source.createTypedArray(Gold.CREATOR);
        setGold(gold);
        diamond = source.createTypedArray(Diamond.CREATOR);
        setDiamond(diamond);
        gem = source.createTypedArray(Gem.CREATOR);
        setGem(gem);
        setCost(source.readString());
        setHold(source.readString());
        setSuccess(source.readString());
        setCompanyId(source.readString());
        setItemOwnerId(source.readString());
    }

    public static final Parcelable.Creator<AllItemResponse> CREATOR = new Parcelable.Creator<AllItemResponse>() {
        @Override
        public AllItemResponse createFromParcel(Parcel source) {
            return new AllItemResponse(source);
        }

        @Override
        public AllItemResponse[] newArray(int size) {
            return new AllItemResponse[size];
        }
    };
}