package com.interwebbingtechonolgy.android_commons_commerce.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class MemberData {

    @SerializedName("company_id")
    String companyId;

    @SerializedName("member_id")
    String memberId;

    @SerializedName("member_name")
    String memberName;

    @SerializedName("member_phone")
    String memberPh;

    @SerializedName("member_level")
    String memberLevel;

    @SerializedName("member_nrc")
    String memberNrc;

    @SerializedName("member_image")
    String memberImage;

    @SerializedName("member_address")
    String memberAddress;

    @SerializedName("member_email")
    String memberEmail;

    @SerializedName("created_at")
    Date createdAt;

    @SerializedName("updated_at")
    Date updatedAt;

    @SerializedName("member_password")
    String memberPassword;

    public MemberData(String companyId, String memberId, String memberName, String memberPh, String memberLevel, String memberNrc, String memberImage, String memberAddress, String memberEmail, Date createdAt, Date updatedAt, String memberPassword) {
        this.companyId = companyId;
        this.memberId = memberId;
        this.memberName = memberName;
        this.memberPh = memberPh;
        this.memberLevel = memberLevel;
        this.memberNrc = memberNrc;
        this.memberImage = memberImage;
        this.memberAddress = memberAddress;
        this.memberEmail = memberEmail;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.memberPassword = memberPassword;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberPh() {
        return memberPh;
    }

    public void setMemberPh(String memberPh) {
        this.memberPh = memberPh;
    }

    public String getMemberLevel() {
        return memberLevel;
    }

    public void setMemberLevel(String memberLevel) {
        this.memberLevel = memberLevel;
    }

    public String getMemberNrc() {
        return memberNrc;
    }

    public void setMemberNrc(String memberNrc) {
        this.memberNrc = memberNrc;
    }

    public String getMemberImage() {
        return memberImage;
    }

    public void setMemberImage(String memberImage) {
        this.memberImage = memberImage;
    }

    public String getMemberAddress() {
        return memberAddress;
    }

    public void setMemberAddress(String memberAddress) {
        this.memberAddress = memberAddress;
    }

    public String getMemberEmail() {
        return memberEmail;
    }

    public void setMemberEmail(String memberEmail) {
        this.memberEmail = memberEmail;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getMemberPassword() {
        return memberPassword;
    }

    public void setMemberPassword(String memberPassword) {
        this.memberPassword = memberPassword;
    }
}
