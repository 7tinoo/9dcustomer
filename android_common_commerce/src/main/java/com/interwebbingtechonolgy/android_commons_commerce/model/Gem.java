package com.interwebbingtechonolgy.android_commons_commerce.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Smart02 on 1/24/18.
 */

public class Gem implements Parcelable {
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getWhere_from() {
        return where_from;
    }

    public void setWhere_from(String where_from) {
        this.where_from = where_from;
    }

    private String type;
    private String count;
    private String quality;
    private String weight;
    private String price;
    private String where_from;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString( type );
        dest.writeString( count );
        dest.writeString( quality );
        dest.writeString( weight );
        dest.writeString( price );
        dest.writeString( where_from );
    }

    public Gem(Parcel source) {
        setType( source.readString() );
        setCount( source.readString() );
        setQuality( source.readString() );
        setWeight( source.readString() );
        setPrice( source.readString() );
        setWhere_from( source.readString() );
    }

    public static final Parcelable.Creator<Gem> CREATOR = new Parcelable.Creator<Gem>() {
        @Override
        public Gem createFromParcel(Parcel source) {
            return new Gem( source );
        }

        @Override
        public Gem[] newArray(int size) {
            return new Gem[size];
        }
    };
}
