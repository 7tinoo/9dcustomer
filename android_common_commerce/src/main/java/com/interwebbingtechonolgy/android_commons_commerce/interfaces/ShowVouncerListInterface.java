package com.interwebbingtechonolgy.android_commons_commerce.interfaces;

import com.interwebbingtechonolgy.android_commons_commerce.model.ShowVouncherResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ShowVouncerListInterface {
    @GET("9dragon/getVouncherData")
    Call<List<ShowVouncherResponse>> getVouncherListR(@Query("company_id") String company_id, @Query("member_nrc") String member_nrc) ;
}