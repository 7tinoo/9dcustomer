package com.interwebbingtechonolgy.android_commons_commerce.interfaces;

import com.interwebbingtechonolgy.android_commons_commerce.model.ShowVouncherDetailResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ShowVouncherDetailRestInterface {
    @GET("9dragon/getHistoryMemberDataByNRC")
    Call<List<ShowVouncherDetailResponse>> getVouncherList(@Query("company_id") String company_id, @Query("member_nrc") String member_nrc) ;
}
