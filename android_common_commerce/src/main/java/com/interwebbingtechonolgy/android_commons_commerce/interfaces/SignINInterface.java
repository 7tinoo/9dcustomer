package com.interwebbingtechonolgy.android_commons_commerce.interfaces;

import com.interwebbingtechonolgy.android_commons_commerce.model.MemberData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SignINInterface {
    @GET("9dragon/signIn")
    Call<MemberData> getSignIn(@Query("member_nrc") String userName,
                               @Query("member_password") String password);
}
