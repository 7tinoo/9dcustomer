package com.interwebbingtechonolgy.android_commons_commerce.interfaces;

import com.interwebbingtechonolgy.android_commons_commerce.model.MemberData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SignInwithEmailInterface {


    @GET("9dragon/signInWithEmail")
    Call<MemberData> getSignInWithEmail(@Query("member_email") String userName,
                               @Query("member_password") String password);
}
