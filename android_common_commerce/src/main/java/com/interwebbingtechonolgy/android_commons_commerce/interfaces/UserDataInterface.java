package com.interwebbingtechonolgy.android_commons_commerce.interfaces;

import com.interwebbingtechonolgy.android_commons_commerce.model.Member;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Ko Mg on 1/17/2018.
 */

public interface UserDataInterface {
    @GET("9dragon/getUserDatabyNrc")
    Call<Member> getUserData(@Query("member_nrc") String user);

}
