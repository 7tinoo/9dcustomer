package com.interwebbingtechonolgy.android_commons_commerce.interfaces;

import com.interwebbingtechonolgy.android_commons_commerce.model.CreateMemberPwdResponse;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface CreateMemberPwdInterface {
    @POST("9dragon/createMemberPassword")
    Call<CreateMemberPwdResponse> createPassword(@Query("member_nrc") String userName,
                                                 @Query("member_password") String confirmPassword);
}
