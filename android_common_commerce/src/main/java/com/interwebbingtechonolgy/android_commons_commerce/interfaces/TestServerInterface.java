package com.interwebbingtechonolgy.android_commons_commerce.interfaces;

import com.interwebbingtechonolgy.android_commons_commerce.model.TestingModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface TestServerInterface {
    @GET("9d/testing")
    Call<TestingModel> getTestData();

}

