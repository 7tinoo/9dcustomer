package com.interwebbingtechonolgy.android_commons_commerce.manager;

import android.content.Context;
import android.util.Log;

import com.interwebbingtechonolgy.android_commons_commerce.event.ShowVouncherReceivedDetailEvent;
import com.interwebbingtechonolgy.android_commons_commerce.model.ApiUrl;
import com.interwebbingtechonolgy.android_commons_commerce.model.ShowVouncherDetailResponse;
import com.interwebbingtechonolgy.android_commons_commerce.request.ShowVouncherDetailRequest;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by smart05 on 6/9/18.
 */

public class ShowVouncherDetailManager implements ShowVouncherDetailRequest.ShowVouncherRequestDelegate {

    private static ShowVouncherDetailManager sManagerInstance;
    private Context mAppContext;
    private List<ShowVouncherDetailResponse> allVouncherList = new ArrayList<>();
    public static String companyId = null;
    public static String NRC = null;


    private ShowVouncherDetailManager(Context context) {
        mAppContext = context;
    }

    public static ShowVouncherDetailManager getInstance() {
        return sManagerInstance;
    }

    public static ShowVouncherDetailManager getInstance(Context context) {
        if (sManagerInstance == null) {
            sManagerInstance = new ShowVouncherDetailManager( context );
        }
        return sManagerInstance;
    }

    public List<ShowVouncherDetailResponse> getAllVouncherList() {

        List<ShowVouncherDetailResponse> kk = new ArrayList<>();

        if (allVouncherList == null) {
            allVouncherList = new ArrayList<>();
        }

        return allVouncherList;
    }

    public void fetchAllVouncherList(String cId, String nrc) {
        companyId = cId;
        NRC = nrc;
//       Log.d( "nnnnppppppp", NRC );
        ShowVouncherDetailRequest showVouncherRequest = new ShowVouncherDetailRequest( this );
        showVouncherRequest.start( ApiUrl.getBaseUrl() );

    }

//    public void fetchAllVouncherList2(String companyIds, String nrcs) {
//        companyId = companyIds;
//        NRC = nrcs;
//        Log.d( "nnnnppppppp", NRC );
//        ShowVouncherDetailRequest showVouncherRequest = new ShowVouncherDetailRequest( this );
//        showVouncherRequest.start( ApiUrl.getBaseUrl() );
//    }

    @Override
    public void onShowVouncherReceived(List<ShowVouncherDetailResponse> allAccountList) {


        allVouncherList.clear();
        allVouncherList.addAll( allAccountList );
        Log.d( "MANAGER", String.valueOf( allAccountList ) );
        EventBus.getDefault().post( new ShowVouncherReceivedDetailEvent( allAccountList ) );

    }
}