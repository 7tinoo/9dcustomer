package com.interwebbingtechonolgy.android_commons_commerce.manager;

import android.content.Context;
import android.util.Log;

import com.interwebbingtechonolgy.android_commons_commerce.event.SignInReceivedEvent;
import com.interwebbingtechonolgy.android_commons_commerce.model.ApiUrl;
import com.interwebbingtechonolgy.android_commons_commerce.model.MemberData;
import com.interwebbingtechonolgy.android_commons_commerce.request.CreateMemberPwdRequest;
import com.interwebbingtechonolgy.android_commons_commerce.request.SignInWithEmailRequest;

import de.greenrobot.event.EventBus;

public class SignInWithEmailManager implements SignInWithEmailRequest.SignInWithEmailDelegate {
    public static String username = null;
    public static String password = null;
    private Context mContext;
    SignInWithEmailRequest signInRequest;
    CreateMemberPwdRequest createMemberPwdRequest;
    public static SignInWithEmailManager signInManager;
    public static MemberData memberData1;

    public SignInWithEmailManager(Context mContext) {
        this.mContext = mContext;
    }

    public static SignInWithEmailManager getInstance() {
        return signInManager;
    }

    public static SignInWithEmailManager getInstance(Context context) {
        if (signInManager == null) {
            signInManager = new SignInWithEmailManager( context );
        }
        return signInManager;
    }

    public void isLogin(String getUserName, String getPassword) {


        username = getUserName;
        password = getPassword;
        Log.d( "kkla", username + password );

        signInRequest = new SignInWithEmailRequest( this );
        signInRequest.start( ApiUrl.getBaseUrl() );
    }

    @Override
    public void onSignInSuccess(MemberData memberData, boolean isSuccess) {
        memberData1 = memberData;
        EventBus.getDefault().post( new SignInReceivedEvent( memberData1, isSuccess ) );
    }


}
