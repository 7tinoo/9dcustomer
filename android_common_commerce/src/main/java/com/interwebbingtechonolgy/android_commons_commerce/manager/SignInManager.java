package com.interwebbingtechonolgy.android_commons_commerce.manager;

import android.content.Context;
import android.util.Log;

import com.interwebbingtechonolgy.android_commons_commerce.event.CreatePwdReceivedEvent;
import com.interwebbingtechonolgy.android_commons_commerce.event.SignInReceivedEvent;
import com.interwebbingtechonolgy.android_commons_commerce.model.ApiUrl;
import com.interwebbingtechonolgy.android_commons_commerce.model.CreateMemberPwdResponse;
import com.interwebbingtechonolgy.android_commons_commerce.model.MemberData;
import com.interwebbingtechonolgy.android_commons_commerce.request.CreateMemberPwdRequest;
import com.interwebbingtechonolgy.android_commons_commerce.request.SignInRequest;

import de.greenrobot.event.EventBus;

public class SignInManager implements SignInRequest.SignInDelegate, CreateMemberPwdRequest.CreateMemberPwdDelegate {
    public static String username = null;
    public static String password = null;
    private Context mContext;
    SignInRequest signInRequest;
    CreateMemberPwdRequest createMemberPwdRequest;
    public static SignInManager signInManager;

    public SignInManager(Context mContext) {
        this.mContext = mContext;
    }

    public static SignInManager getInstance(){
        return signInManager;
    }

    public static SignInManager getInstance(Context context){
        if(signInManager == null){
            signInManager = new SignInManager(context);
        }
        return signInManager;
    }

    public void isLogin(String getUserName, String getPassword) {


        username = getUserName;
        password = getPassword;
        Log.d("kkladdd",username + password );

        signInRequest = new SignInRequest(this);
        signInRequest.start(ApiUrl.getBaseUrl());
    }

    @Override
    public void onSignInSuccess(MemberData memberData, boolean isSuccess) {
        EventBus.getDefault().post(new SignInReceivedEvent(memberData,isSuccess));
    }

    public void createMemberPwd(String getUserName, String password) {
        username = getUserName;
        this.password = password;

        createMemberPwdRequest = new CreateMemberPwdRequest(this);
        createMemberPwdRequest.start(ApiUrl.getBaseUrl());
    }

    @Override
    public void onPwdCreateSuccess(CreateMemberPwdResponse memberPwdResponse,boolean isSuccess) {
        EventBus.getDefault().post(new CreatePwdReceivedEvent(isSuccess));
    }
}
