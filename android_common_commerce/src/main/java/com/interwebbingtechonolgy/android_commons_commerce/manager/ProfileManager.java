package com.interwebbingtechonolgy.android_commons_commerce.manager;

/**
 * Created by Ko Mg on 1/17/2018.
 */

import android.content.Context;

import com.interwebbingtechonolgy.android_commons_commerce.event.ProfileReceivedEvent;
import com.interwebbingtechonolgy.android_commons_commerce.model.ApiUrl;
import com.interwebbingtechonolgy.android_commons_commerce.model.Member;
import com.interwebbingtechonolgy.android_commons_commerce.request.ProfileRequest;

import de.greenrobot.event.EventBus;

/**
 * Created by smart05 on 1/11/18.
 */

public class ProfileManager implements ProfileRequest.ProfileRequestDelegate {

    private static ProfileManager sManagerInstance;
    private Context mAppContext;
    Member allUserResponses = null;
    public static String userId = null;
    public static String cid = null;
    ProfileRequest profileRequest;


    private ProfileManager(Context context) {
        mAppContext = context;
    }

    public static ProfileManager getInstance() {
        return sManagerInstance;
    }

    public static ProfileManager getInstance(Context context) {
        if (sManagerInstance == null) {
            sManagerInstance = new ProfileManager( context );
        }
        return sManagerInstance;
    }

    public Member getUserList() {

        return allUserResponses;
    }

    public void fetchAllItemList(String id) {
        userId = id;
        profileRequest = new ProfileRequest( this );
        profileRequest.start( ApiUrl.getBaseUrl() );

    }

    @Override
    public void onProfileReceived(Member UserResponse) {

        allUserResponses = UserResponse;
        EventBus.getDefault().post( new ProfileReceivedEvent( UserResponse ) );
    }
}