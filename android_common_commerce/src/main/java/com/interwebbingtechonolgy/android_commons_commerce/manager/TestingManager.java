package com.interwebbingtechonolgy.android_commons_commerce.manager;

import android.content.Context;

import com.interwebbingtechonolgy.android_commons_commerce.event.TestingEvent;
import com.interwebbingtechonolgy.android_commons_commerce.model.ApiUrl;
import com.interwebbingtechonolgy.android_commons_commerce.model.TestingModel;
import com.interwebbingtechonolgy.android_commons_commerce.request.TestServerRequest;

import de.greenrobot.event.EventBus;

public class TestingManager implements TestServerRequest.TestSErverDelegate {

    public static TestingManager testingManager;
    public TestServerRequest testSErverRequest;
    Context context;



    public TestingManager(Context context) {
        this.context = context;
    }

    public static TestingManager getInstance() {
        return testingManager;
    }

    public static TestingManager getInstance(Context context) {
        if (testingManager == null) {
            testingManager = new TestingManager( context );
        }
        return testingManager;
    }

    public void restaurntData() {
        testSErverRequest = new TestServerRequest(this);
        testSErverRequest.start( ApiUrl.getBaseUrl());
    }


    @Override
    public void restaurntDataSuccess(TestingModel restaurantDataResponse) {
        EventBus.getDefault().post( new TestingEvent(restaurantDataResponse));
    }
}
