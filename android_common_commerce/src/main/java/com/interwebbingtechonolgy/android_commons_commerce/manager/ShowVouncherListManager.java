package com.interwebbingtechonolgy.android_commons_commerce.manager;

import android.content.Context;
import android.util.Log;

import com.interwebbingtechonolgy.android_commons_commerce.event.ShowVouncherReceivedEvent;
import com.interwebbingtechonolgy.android_commons_commerce.model.ApiUrl;
import com.interwebbingtechonolgy.android_commons_commerce.model.ShowVouncherResponse;
import com.interwebbingtechonolgy.android_commons_commerce.request.ShowVouncherListRequest;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

public class ShowVouncherListManager implements ShowVouncherListRequest.ShowVouncherRequestDelegate {

    private static ShowVouncherListManager sManagerInstance;
    private Context mAppContext;
    private List<ShowVouncherResponse> allVouncherList = new ArrayList<>();
    public static String companyId = null;
    public static String NRC = null;


    private ShowVouncherListManager(Context context) {
        mAppContext = context;
    }

    public static ShowVouncherListManager getInstance() {
        return sManagerInstance;
    }

    public static ShowVouncherListManager getInstance(Context context) {
        if (sManagerInstance == null) {
            sManagerInstance = new ShowVouncherListManager( context );
        }
        return sManagerInstance;
    }

    public List<ShowVouncherResponse> getAllVouncherList() {

        List<ShowVouncherResponse> kk = new ArrayList<>();

        if (allVouncherList == null) {
            allVouncherList = new ArrayList<>();
        }

        return allVouncherList;
    }

    public void fetchAllVouncherList(String cId, String nrc) {
        companyId = cId;
        NRC = nrc;
//        Log.d( "SHowVDetailManager", NRC );
        ShowVouncherListRequest showVouncherRequest = new ShowVouncherListRequest( this );
        showVouncherRequest.start( ApiUrl.getBaseUrl() );

    }

    @Override
    public void onShowVouncherReceived(List<ShowVouncherResponse> allAccountList) {


        allVouncherList.clear();
        allVouncherList.addAll( allAccountList );
        Log.d( "MANAGER", String.valueOf( allAccountList ) );
        EventBus.getDefault().post( new ShowVouncherReceivedEvent( allAccountList ) );

    }
}