package com.interwebbingtechonolgy.android_commons_commerce.request;

import android.util.Log;

import com.interwebbingtechonolgy.android_commons_commerce.interfaces.ShowVouncherDetailRestInterface;
import com.interwebbingtechonolgy.android_commons_commerce.manager.ShowVouncherDetailManager;
import com.interwebbingtechonolgy.android_commons_commerce.model.ShowVouncherDetailResponse;
import com.interwebbingtechonolgy.android_commons_commerce.network.RequestBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by smart05 on 6/9/18.
 */

public class ShowVouncherDetailRequest implements Callback<List<ShowVouncherDetailResponse>> {

    private ShowVouncherDetailRequest.ShowVouncherRequestDelegate mDelegate;
    private String company_id = ShowVouncherDetailManager.companyId;
    private String member_nrc= ShowVouncherDetailManager.NRC;

    public ShowVouncherDetailRequest(ShowVouncherDetailRequest.ShowVouncherRequestDelegate deliverListRequestDelegate) {
        mDelegate = deliverListRequestDelegate;
    }

    public void start(String Url) {

        Log.d("showvdetailoooooo", ShowVouncherDetailManager.companyId );
//     Log.d("showvrdetail00000000",ShowVouncherDetailManager.NRC);

        ShowVouncherDetailRestInterface showVouncherDetailRestInterface = RequestBuilder.createService(ShowVouncherDetailRestInterface.class,Url);
        Call<List<ShowVouncherDetailResponse>> call = showVouncherDetailRestInterface.getVouncherList(ShowVouncherDetailManager.companyId, ShowVouncherDetailManager.NRC);
        Log.d(TAG,call + "Call");
        call.enqueue(this);

    }

    @Override
    public void onResponse(Call<List<ShowVouncherDetailResponse>> call, Response<List<ShowVouncherDetailResponse>> response) {


        Log.e("helloBaby" , String.valueOf(response.body().size()));
        List<ShowVouncherDetailResponse> deliverDataList = response.body();
        if(response.body()!=null){

            mDelegate.onShowVouncherReceived(deliverDataList);

        }
    }

    @Override
    public void onFailure(Call<List<ShowVouncherDetailResponse>> call, Throwable t) {
        Log.e("member level", String.valueOf(t.getMessage()));
    }

    public interface ShowVouncherRequestDelegate {
        void onShowVouncherReceived(List<ShowVouncherDetailResponse> vouncherResponses);

    }
}