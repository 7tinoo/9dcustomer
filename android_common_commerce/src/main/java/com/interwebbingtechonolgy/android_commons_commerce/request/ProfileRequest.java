package com.interwebbingtechonolgy.android_commons_commerce.request;


import android.util.Log;

import com.interwebbingtechonolgy.android_commons_commerce.interfaces.SignINInterface;
import com.interwebbingtechonolgy.android_commons_commerce.interfaces.UserDataInterface;
import com.interwebbingtechonolgy.android_commons_commerce.manager.ProfileManager;
import com.interwebbingtechonolgy.android_commons_commerce.model.Member;
import com.interwebbingtechonolgy.android_commons_commerce.model.MemberData;
import com.interwebbingtechonolgy.android_commons_commerce.network.RequestBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.ContentValues.TAG;

public class ProfileRequest implements Callback<Member> {
    private ProfileRequest.ProfileRequestDelegate mDelegate;

    public ProfileRequest(ProfileRequest.ProfileRequestDelegate profileRequestDelegate) {
        mDelegate = profileRequestDelegate;
    }


    public void start(String Url) {
        UserDataInterface allUserRestInterface = RequestBuilder.createService(UserDataInterface.class,Url);
        Call<Member> call = allUserRestInterface.getUserData( ProfileManager.userId );
        call.enqueue(this);
    }

  @Override
    public void onResponse(Call<Member> call, Response<Member> response) {
//        Log.d("Hello",response.body().getEmail());
        //  System.out.print(response.body().getEmail());
        Member userDataList = response.body();
        if (response.body() != null) {
            mDelegate.onProfileReceived( userDataList );
        } else {
            System.out.println( "))))))))))))" );
        }
    }


    @Override
    public void onFailure(Call<Member> call, Throwable t) {
        //start(ApiUrl.getBackupUrl());
    }

    public interface ProfileRequestDelegate {
        void onProfileReceived(Member account);

    }
}