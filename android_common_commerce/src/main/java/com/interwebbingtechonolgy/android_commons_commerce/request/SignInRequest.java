package com.interwebbingtechonolgy.android_commons_commerce.request;

import android.util.Log;

import com.interwebbingtechonolgy.android_commons_commerce.interfaces.SignINInterface;
import com.interwebbingtechonolgy.android_commons_commerce.interfaces.SignInwithEmailInterface;
import com.interwebbingtechonolgy.android_commons_commerce.manager.SignInManager;
import com.interwebbingtechonolgy.android_commons_commerce.model.MemberData;
import com.interwebbingtechonolgy.android_commons_commerce.network.RequestBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInRequest implements Callback<MemberData> {
    private String userName = SignInManager.username;
    private String password = SignInManager.password;
    private SignInDelegate mDelegate;
    private SignINInterface signINInterface;
    private SignInwithEmailInterface signInwithEmailInterface;
    private String TAG = "SignIn";

    public SignInRequest(SignInDelegate mDelegate) {
        this.mDelegate = mDelegate;
    }

    public void start(String baseUrl) {
        signINInterface = RequestBuilder.createService(SignINInterface.class,baseUrl);
        Call<MemberData> call = signINInterface.getSignIn(userName,password);
        Log.d(TAG,call + "Call");
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<MemberData> call, Response<MemberData> response) {
        Log.e(TAG, response.body()+"onResponse");
        if(response.body() != null && response.isSuccessful()){
            mDelegate.onSignInSuccess(response.body(),true);
        }
        else {
            mDelegate.onSignInSuccess(response.body(),false);
        }
    }

    @Override
    public void onFailure(Call<MemberData> call, Throwable t) {
        Log.d(TAG,t.getMessage());
    }

    public interface SignInDelegate {
        void onSignInSuccess(MemberData memberData,boolean isSuccess);
    }
}
