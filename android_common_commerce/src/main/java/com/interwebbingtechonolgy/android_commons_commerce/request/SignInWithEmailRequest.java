package com.interwebbingtechonolgy.android_commons_commerce.request;

import android.util.Log;

import com.interwebbingtechonolgy.android_commons_commerce.interfaces.SignInwithEmailInterface;
import com.interwebbingtechonolgy.android_commons_commerce.manager.SignInWithEmailManager;
import com.interwebbingtechonolgy.android_commons_commerce.model.MemberData;
import com.interwebbingtechonolgy.android_commons_commerce.network.RequestBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInWithEmailRequest implements Callback<MemberData> {
    private String userName = SignInWithEmailManager.username;
    private String password = SignInWithEmailManager.password;
    private SignInWithEmailDelegate mDelegate;

    private SignInwithEmailInterface signInwithEmailInterface;
    private String TAG = "SignIn";

    public SignInWithEmailRequest(SignInWithEmailDelegate mDelegate) {
        this.mDelegate = mDelegate;
    }

    public void start(String baseUrl) {
        signInwithEmailInterface = RequestBuilder.createService( SignInwithEmailInterface.class, baseUrl );
        Log.d( "kkla@", userName + password );

        Call<MemberData> call = signInwithEmailInterface.getSignInWithEmail( userName, password );
        Log.d( TAG, call + "Call" );
        call.enqueue( this );
    }

    @Override
    public void onResponse(Call<MemberData> call, Response<MemberData> response) {
        Log.e( TAG, response.body() + "onResponse" );
        MemberData data = response.body();
        if (response.body() != null && response.isSuccessful()) {
            mDelegate.onSignInSuccess( response.body(), true );
        } else {
            mDelegate.onSignInSuccess( response.body(), false );
        }
    }

    @Override
    public void onFailure(Call<MemberData> call, Throwable t) {
        Log.d( TAG, t.getMessage() );
    }

    public interface SignInWithEmailDelegate {
        void onSignInSuccess(MemberData memberData, boolean isSuccess);
    }
}
