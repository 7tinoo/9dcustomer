package com.interwebbingtechonolgy.android_commons_commerce.request;

import android.util.Log;

import com.interwebbingtechonolgy.android_commons_commerce.interfaces.CreateMemberPwdInterface;
import com.interwebbingtechonolgy.android_commons_commerce.manager.SignInManager;
import com.interwebbingtechonolgy.android_commons_commerce.model.CreateMemberPwdResponse;
import com.interwebbingtechonolgy.android_commons_commerce.network.RequestBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateMemberPwdRequest implements Callback<CreateMemberPwdResponse> {
    private String userName = SignInManager.username;
    private String password = SignInManager.password;
    private CreateMemberPwdDelegate mDelegate;
    private CreateMemberPwdInterface createPwdInterface;
    private String TAG = "CreatePassword";

    public CreateMemberPwdRequest(CreateMemberPwdDelegate mDelegate) {
        this.mDelegate = mDelegate;
    }

    @Override
    public void onResponse(Call<CreateMemberPwdResponse> call, Response<CreateMemberPwdResponse> response) {
        Log.d(TAG,response.body().isSuccess()+"@@@@@");
        if(response.body() != null){
            mDelegate.onPwdCreateSuccess(response.body(),true);
        }
        else {
            mDelegate.onPwdCreateSuccess(response.body(),false);
        }
    }

    @Override
    public void onFailure(Call<CreateMemberPwdResponse> call, Throwable t) {
        Log.e(TAG,t.getMessage());
    }

    public void start(String baseUrl) {
        createPwdInterface = RequestBuilder.createService(CreateMemberPwdInterface.class,baseUrl);
        Call<CreateMemberPwdResponse> call = createPwdInterface.createPassword(userName,password);
        Log.e(TAG,call+"******");
        call.enqueue(this);
    }

    public interface CreateMemberPwdDelegate {
        void onPwdCreateSuccess(CreateMemberPwdResponse isSuccess,boolean success);
    }
}
