package com.interwebbingtechonolgy.android_commons_commerce.request;

import android.util.Log;

import com.interwebbingtechonolgy.android_commons_commerce.interfaces.ShowVouncerListInterface;
import com.interwebbingtechonolgy.android_commons_commerce.manager.ShowVouncherDetailManager;
import com.interwebbingtechonolgy.android_commons_commerce.model.ShowVouncherResponse;
import com.interwebbingtechonolgy.android_commons_commerce.network.RequestBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class ShowVouncherListRequest implements Callback<List<ShowVouncherResponse>> {

    private ShowVouncherListRequest.ShowVouncherRequestDelegate mDelegate;
    private String company_id = ShowVouncherDetailManager.companyId;
    private String member_nrc= ShowVouncherDetailManager.NRC;

    public ShowVouncherListRequest(ShowVouncherListRequest.ShowVouncherRequestDelegate deliverListRequestDelegate) {
        mDelegate = deliverListRequestDelegate;
    }

    public void start(String Url) {

        ShowVouncerListInterface showVouncherDetailRestInterface = RequestBuilder.createService(ShowVouncerListInterface.class,Url);
        Call<List<ShowVouncherResponse>> call = showVouncherDetailRestInterface.getVouncherListR(ShowVouncherDetailManager.companyId, ShowVouncherDetailManager.NRC);
        Log.d(TAG,call + "Call");
        call.enqueue(this);

    }

    @Override
    public void onResponse(Call<List<ShowVouncherResponse>> call, Response<List<ShowVouncherResponse>> response) {


//        Log.e("helloBaby" , String.valueOf(response.body().size()));
        List<ShowVouncherResponse> deliverDataList = response.body();
        if(response.body()!=null){

            mDelegate.onShowVouncherReceived(deliverDataList);

        }
    }

    @Override
    public void onFailure(Call<List<ShowVouncherResponse>> call, Throwable t) {
        Log.e("member level", String.valueOf(t.getMessage()));
    }

    public interface ShowVouncherRequestDelegate {
        void onShowVouncherReceived(List<ShowVouncherResponse> vouncherResponses);

    }
}