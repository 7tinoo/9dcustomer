package com.interwebbingtechonolgy.android_commons_commerce.event;

import com.interwebbingtechonolgy.android_commons_commerce.model.MemberData;

public class SignInReceivedEvent {
    MemberData memberData;
    boolean isSuccess;

    public SignInReceivedEvent(MemberData memberData,boolean isSuccess) {
        this.memberData = memberData;
        this.isSuccess = isSuccess;
    }

    public MemberData getMemberData() {
        return memberData;
    }

    public void setMemberData(MemberData memberData) {
        this.memberData = memberData;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }
}
