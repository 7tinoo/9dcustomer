package com.interwebbingtechonolgy.android_commons_commerce.event;

import com.interwebbingtechonolgy.android_commons_commerce.model.Member;

/**
 * Created by Ko Mg on 1/18/2018.
 */

public class ProfileReceivedEvent {

    private Member userModel;

    public Member getUserModel() {
        return userModel;
    }

    public ProfileReceivedEvent(Member userModel) {

        this.userModel = userModel;
    }
}
