package com.interwebbingtechonolgy.android_commons_commerce.event;


public class CreatePwdReceivedEvent {
    boolean isSuccess;

    public CreatePwdReceivedEvent(boolean isSuccess) {
        isSuccess = isSuccess;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }
}
