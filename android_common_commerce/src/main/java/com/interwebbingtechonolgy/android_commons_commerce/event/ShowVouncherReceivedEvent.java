package com.interwebbingtechonolgy.android_commons_commerce.event;

import com.interwebbingtechonolgy.android_commons_commerce.model.ShowVouncherResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by smart05 on 6/11/18.
 */

public class ShowVouncherReceivedEvent extends ArrayList<ShowVouncherResponse> {
   List<ShowVouncherResponse> showVouncherResponse;


    public ShowVouncherReceivedEvent(List<ShowVouncherResponse> showVouncherResponse) {
        this.showVouncherResponse = showVouncherResponse;
    }

    public List<ShowVouncherResponse> getShowVouncherResponse() {
        return showVouncherResponse;
    }

    public void setShowVouncherResponse(List<ShowVouncherResponse> showVouncherResponse) {
        this.showVouncherResponse = showVouncherResponse;
    }
}
