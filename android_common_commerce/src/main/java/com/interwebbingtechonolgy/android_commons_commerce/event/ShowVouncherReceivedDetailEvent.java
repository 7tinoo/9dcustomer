package com.interwebbingtechonolgy.android_commons_commerce.event;

import com.interwebbingtechonolgy.android_commons_commerce.model.ShowVouncherDetailResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by smart05 on 6/11/18.
 */

public class ShowVouncherReceivedDetailEvent extends ArrayList<ShowVouncherDetailResponse> {
    List<ShowVouncherDetailResponse> showVouncherResponse;


    public ShowVouncherReceivedDetailEvent(List<ShowVouncherDetailResponse> showVouncherResponse) {
        this.showVouncherResponse = showVouncherResponse;
    }

    public List<ShowVouncherDetailResponse> getShowVouncherResponse() {
        return showVouncherResponse;
    }

    public void setShowVouncherResponse(List<ShowVouncherDetailResponse> showVouncherResponse) {
        this.showVouncherResponse = showVouncherResponse;
    }
}
