package com.interwebbingtechonolgy.android_commons_commerce.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharepreferenceLoginCode {

    public static final String LOGGED_IN_PREF = "logged_in_status";

    static SharedPreferences getPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

//    public static void setLoggedIn(Context context, boolean loggedIn) {
//        SharedPreferences.Editor editor = getPreferences(context).edit();
//        editor.putBoolean(LOGGED_IN_PREF, loggedIn);
//        editor.apply();
//    }
//
//    public static boolean getLoggedStatus(Context context) {
//        return getPreferences(context).getBoolean(LOGGED_IN_PREF, false);
//    }



    public static String readSharedSetting(Context context,String settingName,boolean defaultvalue){

        SharedPreferences preferences=context.getSharedPreferences(LOGGED_IN_PREF,Context.MODE_PRIVATE);
        return preferences.getString(settingName, String.valueOf(defaultvalue));

    }
//    public static String readSharedSetting(Context context,String settingName,boolean defaultvalue){
//
//        SharedPreferences preferences=context.getSharedPreferences(LOGGED_IN_PREF,Context.MODE_PRIVATE);
//        return preferences.getString(settingName, String.valueOf(defaultvalue));
//
//    }

    public static void saveSharedSetting(Context context,String settingName,boolean settingValue){
        SharedPreferences preferences=context.getSharedPreferences(LOGGED_IN_PREF,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=preferences.edit();
        editor.putString(settingName, String.valueOf(settingValue));
        editor.apply();
    }

    public static void SharedPrefesSave(Context context,String Name){
        SharedPreferences preferences=context.getSharedPreferences("NAME",0);
        SharedPreferences.Editor editor=preferences.edit();
        editor.putString("Name", Name);
        editor.commit();
    }



}
