package com.interwebbingtechonolgy.android_commons_commerce.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Set;

/**
 * Created by smart04 on 6/12/17.
 */

public class SharedPreferencesModule
{
    private SharedPreferences mPreferences;

    SharedPreferencesModule(Context context, String name)
    {
        mPreferences = context.getSharedPreferences(name, 0);
    }

    SharedPreferencesModule(Context context)
    {
        mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * enter a new key-boolean pair
     * @param key the unique key
     * @param value the value to enter
     */
    public void put(String key, boolean value)
    {
        mPreferences.edit().putBoolean(key, value).apply();
    }

    /**
     * enter a new key-float pair
     * @param key the unique key
     * @param value the value to enter
     */
    public void put(String key, float value)
    {
        mPreferences.edit().putFloat(key, value).apply();
    }

    /**
     * enter a new key-int pair
     * @param key the unique key
     * @param value the value to enter
     */
    public void put(String key, int value)
    {
        mPreferences.edit().putInt(key, value).apply();
    }

    /**
     * enter a new key-long pair
     * @param key the unique key
     * @param value the value to enter
     */
    public void put(String key, long value)
    {
        mPreferences.edit().putLong(key, value).apply();
    }

    /**
     * enter a new key-string pair
     * @param key the unique key
     * @param value the value to enter
     */
    public void put(String key, String value)
    {
        mPreferences.edit().putString(key, value).apply();
    }
    /**
     * enter a new key-stringSet pair
     * @param key the unique key
     * @param value the value to enter
     */
    public void put(String key, Set<String> value)
    {
        mPreferences.edit().putStringSet(key, value).apply();
    }

    /**
     * get a boolean value
     * @param key the unique key
     * @param defaultValue value if key doesn't exist in preference
     * @return the value if found or {@code defaultValue} otherwise
     */
    public boolean get(String key, boolean defaultValue)
    {
        return mPreferences.getBoolean(key, defaultValue);
    }

    /**
     * get a float value
     * @param key the unique key
     * @param defaultValue value if key doesn't exist in preference
     * @return the value if found or {@code defaultValue} otherwise
     */
    public float get(String key, float defaultValue)
    {
        return mPreferences.getFloat(key, defaultValue);
    }

    /**
     * get an integer value
     * @param key the unique key
     * @param defaultValue value if key doesn't exist in preference
     * @return the value if found or {@code defaultValue} otherwise
     */
    public int get(String key, int defaultValue)
    {
        return mPreferences.getInt(key, defaultValue);
    }

    /**
     * get a long value
     * @param key the unique key
     * @param defaultValue value if key doesn't exist in preference
     * @return the value if found or {@code defaultValue} otherwise
     */
    public long get(String key, long defaultValue)
    {
        return mPreferences.getLong(key, defaultValue);
    }

    /**
     * get a String value
     * @param key the unique key
     * @param defaultValue value if key doesn't exist in preference
     * @return the value if found or {@code defaultValue} otherwise
     */
    public String get(String key, String defaultValue)
    {
        return mPreferences.getString(key, defaultValue);
    }

    /**
     * get a String set value
     * @param key the unique key
     * @param defaultValue value if key doesn't exist in preference
     * @return the value if found or {@code defaultValue} otherwise
     */
    public Set<String> get(String key, Set<String> defaultValue)
    {
        return mPreferences.getStringSet(key, defaultValue);
    }

    /**
     * check if a key exists in the preference
     * @param key the key to check
     * @return {@code true} if key has value, {@code false} otherwise
     */
    public boolean has(String key)
    {
        return mPreferences.contains(key);
    }

    /**
     * removes a key-value pair from the preference
     * @param key the key to remove
     */
    public void remove(String key)
    {
        mPreferences.edit().remove(key).apply();
    }

    /**
     * deletes all key-value pairs in the module
     */
    public void clear()
    {
        mPreferences.edit().clear().apply();
    }
}
