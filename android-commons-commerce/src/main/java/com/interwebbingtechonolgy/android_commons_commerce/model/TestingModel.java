package com.interwebbingtechonolgy.android_commons_commerce.model;

import com.google.gson.annotations.SerializedName;

public class TestingModel {
    @SerializedName("item_name")
    private String itemName;
    @SerializedName("item_code")
    private String itemCode;
    @SerializedName("item_type")
    private String itemType;
    @SerializedName("item_image")
    private String itemImage;

    public TestingModel(String itemName, String itemCode, String itemType, String itemImage) {
        this.itemName = itemName;
        this.itemCode = itemCode;
        this.itemType = itemType;
        this.itemImage = itemImage;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }
}
