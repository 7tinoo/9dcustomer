package com.interwebbingtechonolgy.android_commons_commerce.request;

import android.util.Log;

import com.interwebbingtechonolgy.android_commons_commerce.interfaces.TestServerInterface;
import com.interwebbingtechonolgy.android_commons_commerce.model.TestingModel;
import com.interwebbingtechonolgy.android_commons_commerce.network.RequestBuilder;

import retrofit2.Callback;

import retrofit2.Call;
import retrofit2.Response;

public class TestServerRequest implements Callback<TestingModel> {


    TestSErverDelegate mDelegate;
    TestServerInterface restaurntDataInterface;


    public TestServerRequest(TestSErverDelegate delegate) {
        this.mDelegate = delegate;
    }

    @Override
    public void onResponse(Call<TestingModel> call, Response<TestingModel> response) {
        TestingModel restaurantDataModels = response.body();


        Log.d( "response ", String.valueOf( restaurantDataModels ) );
        if (response.isSuccessful()) {
            mDelegate.restaurntDataSuccess( restaurantDataModels );

        }
    }

    @Override
    public void onFailure(Call<TestingModel> call, Throwable t) {

    }


    public void start(String baseUrl) {
        restaurntDataInterface = RequestBuilder.createService( TestServerInterface.class, baseUrl );

        Call call = restaurntDataInterface.getTestData();


        call.enqueue( (retrofit2.Callback) this );
    }

    public interface TestSErverDelegate {
        void restaurntDataSuccess(TestingModel restaurantDataModels);
    }


}
