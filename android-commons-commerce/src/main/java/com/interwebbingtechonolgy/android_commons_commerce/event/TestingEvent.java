package com.interwebbingtechonolgy.android_commons_commerce.event;

import com.interwebbingtechonolgy.android_commons_commerce.model.TestingModel;

public class TestingEvent {

    TestingModel testingModel;


    public TestingEvent(TestingModel testingModel) {
        this.testingModel = testingModel;
    }

    public TestingModel getTestingModel() {
        return testingModel;
    }

    public void setTestingModel(TestingModel testingModel) {
        this.testingModel = testingModel;
    }
}
